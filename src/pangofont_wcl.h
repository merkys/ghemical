// PANGOFONT_WCL.H : a wcl with added OpenGL text rendering capability.

// Copyright (C) 2008 Tommi Hassinen, Naosumi Yasufuku.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ghemicalconfig2.h"

#ifndef PANGOFONT_WCL_H
#define PANGOFONT_WCL_H

#include <gtk/gtk.h>

#include <oglappth/base_wcl.h>

/*################################################################################################*/

class pangofont_wcl :
	public base_wcl
{
	private:
	
	gchar * font_string;
	gint font_height;
	
	GLuint font_list_base;
	
	public:
	
	pangofont_wcl(ogl_camera *);
	virtual ~pangofont_wcl(void);
	
	// the OpenGL text rendering methods...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// the implementation is taken from gtkglext-1.2/examples/font.c by Naosumi Yasufuku.
	
	void ogl_InitPangoFont(const gchar *);
	
	int ogl_GetStringWidth(const char *);
	void ogl_WriteString2D(const char *, GLfloat, GLfloat);
	void ogl_WriteString3D(const char *, GLfloat, GLfloat, GLfloat);
};

/*################################################################################################*/

#endif	// PANGOFONT_WCL_H

// eof

// OGLVIEW_WCL.H : write a short description here...

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ghemicalconfig2.h"

#ifndef OGLVIEW_WCL_H
#define OGLVIEW_WCL_H

class oglview_wcl;

#include <ghemical/typedef.h>

#include "pangofont_wcl.h"
#include "custom_camera.h"
#include "project.h"

/*################################################################################################*/

class oglview_wcl :
	public pangofont_wcl
{
	protected:
	
	custom_camera * ccam;
	i32s my_wnd_number;
	
	i32s render; i32s label;
	color_mode * colormode;
	
	iGLu * select_buffer;
	
	bool enable_fog;
	bool accumulate;
	
	static bool quick_update;	// some static flags...
	static bool draw_info;		// some static flags...
	
	float animX;
	float animY;
	
	friend class custom_camera;
	friend class project;
	
	friend class gtk_app;
	friend class gtk_project;
	friend class gtk_oglview_wnd;	// timer + ???
	
	friend class w32_app;
	friend class w32_project;
	friend class w32_oglview_wnd;	// ???
	friend class w32_wnd;		// timer
	
	public:
	
	enum pmode { DoNotPick = 0, pDraw = 1, pErase = 2, pSelect = 3, pMeasure = 4 };
	
	oglview_wcl(custom_camera *);
	virtual ~oglview_wcl(void);
	
	custom_camera * GetCCam(void);
	
	void GetCRD(i32s *, fGL *);
	
	void ButtonEvent(int, int);		// virtual
	void MotionEvent(int, int);		// virtual
	
	void UpdateWnd(void);			// virtual
	
// here we sometimes need to pass extra parameters to UpdateWnd().
// but UpdateWnd() is defined at liboglappth and so it should be as
// universal as possible -> make a separate method with extra params.
	
	void MyUpdateWnd(pmode = DoNotPick, int = 0, int = 0);
	
	void InitGL(void);			// virtual
	void RenderGL(rmode);			// virtual
};

/*################################################################################################*/

#endif	// OGLVIEW_WCL_H

// eof

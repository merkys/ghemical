// CUSTOM_CAMERA.CPP

// Copyright (C) 2006 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "custom_camera.h"	// config.h is here -> we get ENABLE-macros here...

//#include "base_app.h"

#include <sstream>
using namespace std;

/*################################################################################################*/

int custom_camera::ccam_counter = 0;

custom_camera::custom_camera(const ogl_object_location & p1, GLfloat p2, project * p3) :
	ogl_camera(p1, p2)
{
	prj = p3;
	
	ccam_index = ++ccam_counter;
	wcl_counter = 0;
}

custom_camera::~custom_camera(void)
{
}

/*################################################################################################*/

// eof

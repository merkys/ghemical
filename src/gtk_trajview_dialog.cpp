// GTK_TRAJVIEW_DIALOG.CPP

// Copyright (C) 2002 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_trajview_dialog.h"

#include "local_i18n.h"

#include <sstream>
#include <iostream>
using namespace std;

/*################################################################################################*/

gtk_trajview_dialog::gtk_trajview_dialog(gtk_project * p1) :
	gtk_glade_dialog("glade/gtk_trajview_dialog.glade")
{
	prj = p1;
	
	dialog = glade_xml_get_widget(xml, "trajview_dialog");
	if (dialog == NULL)
	{
		cout << _("WARNING : trajview_dialog : glade_xml_get_widget() failed!!!") << endl;
		return;
	}
	
	// connect the handlers...
	
	glade_xml_signal_connect_data(xml, "on_dialog_destroy", (GtkSignalFunc) handler_Destroy, (gpointer) this);
	
	glade_xml_signal_connect_data(xml, "on_button_begin_clicked", (GtkSignalFunc) handler_ButtonBegin, (gpointer) this);
	glade_xml_signal_connect_data(xml, "on_button_prev_clicked", (GtkSignalFunc) handler_ButtonPrev, (gpointer) this);
	glade_xml_signal_connect_data(xml, "on_button_play_clicked", (GtkSignalFunc) handler_ButtonPlay, (gpointer) this);
	glade_xml_signal_connect_data(xml, "on_button_next_clicked", (GtkSignalFunc) handler_ButtonNext, (gpointer) this);
	glade_xml_signal_connect_data(xml, "on_button_end_clicked", (GtkSignalFunc) handler_ButtonEnd, (gpointer) this);
	
	glade_xml_signal_connect_data(xml, "on_button_close_clicked", (GtkSignalFunc) handler_ButtonClose, (gpointer) this);
	
	gtk_widget_show(dialog);	// MODELESS
}

gtk_trajview_dialog::~gtk_trajview_dialog(void)
{
//	prj->CloseTrajectory();	// ???
	
	// see handler_ButtonClose() below...
	// see handler_ButtonClose() below...
	// see handler_ButtonClose() below...
}

void gtk_trajview_dialog::handler_Destroy(GtkWidget *, gpointer data)
{
	gtk_trajview_dialog * ref = (gtk_trajview_dialog *) data;
	cout << "handler_Destroy() : ref = " << ref << endl;
}

void gtk_trajview_dialog::handler_ButtonBegin(GtkWidget *, gpointer data)
{
	gtk_trajview_dialog * ref = (gtk_trajview_dialog *) data;
	
	i32s frame = 0;
	ref->prj->SetCurrentFrame(frame);
	ref->prj->ReadTrajectoryFrame();
	
	ostringstream str1;
	str1 << _("frame = ") << ref->prj->GetCurrentFrame() << endl << ends;
	ref->prj->PrintToLog(str1.str().c_str());
	
	ref->prj->UpdateAllGraphicsViews(true);
}

void gtk_trajview_dialog::handler_ButtonPrev(GtkWidget *, gpointer data)
{
	gtk_trajview_dialog * ref = (gtk_trajview_dialog *) data;
	
	i32s frame = ref->prj->GetCurrentFrame() - 1;
	i32s max = ref->prj->GetTotalFrames();
	
	if (frame < 0) frame = 0;
	if (frame >= max) frame = max - 1;
	
	ref->prj->SetCurrentFrame(frame);
	ref->prj->ReadTrajectoryFrame();
	
	ostringstream str1;
	str1 << _("frame = ") << ref->prj->GetCurrentFrame() << endl << ends;
	ref->prj->PrintToLog(str1.str().c_str());
	
	ref->prj->UpdateAllGraphicsViews(true);
}

void gtk_trajview_dialog::handler_ButtonPlay(GtkWidget *, gpointer data)
{
	gtk_trajview_dialog * ref = (gtk_trajview_dialog *) data;

	i32s max = ref->prj->GetTotalFrames();
	for (i32s loop = 0;loop < max;loop++)
	{
		ref->prj->SetCurrentFrame(loop);
		ref->prj->ReadTrajectoryFrame();
		
		ref->prj->UpdateAllGraphicsViews(true);
	}
}

void gtk_trajview_dialog::handler_ButtonNext(GtkWidget *, gpointer data)
{
	gtk_trajview_dialog * ref = (gtk_trajview_dialog *) data;

	i32s frame = ref->prj->GetCurrentFrame() + 1;
	i32s max = ref->prj->GetTotalFrames();
	
	if (frame < 0) frame = 0;
	if (frame >= max) frame = max - 1;
	
	ref->prj->SetCurrentFrame(frame);
	ref->prj->ReadTrajectoryFrame();
	
	ostringstream str1;
	str1 << _("frame = ") << ref->prj->GetCurrentFrame() << endl << ends;
	ref->prj->PrintToLog(str1.str().c_str());
	
	ref->prj->UpdateAllGraphicsViews(true);
}

void gtk_trajview_dialog::handler_ButtonEnd(GtkWidget *, gpointer data)
{
	gtk_trajview_dialog * ref = (gtk_trajview_dialog *) data;

	i32s frame = ref->prj->GetTotalFrames() - 1;
	ref->prj->SetCurrentFrame(frame);
	ref->prj->ReadTrajectoryFrame();
	
	ostringstream str1;
	str1 << _("frame = ") << ref->prj->GetCurrentFrame() << endl << ends;
	ref->prj->PrintToLog(str1.str().c_str());
	
	ref->prj->UpdateAllGraphicsViews(true);
}

void gtk_trajview_dialog::handler_ButtonClose(GtkWidget *, gpointer data)
{
	gtk_trajview_dialog * ref = (gtk_trajview_dialog *) data;
	cout << "handler_ButtonClose() : ref = " << ref << endl;
	
	// close the dialog...
	// ^^^^^^^^^^^^^^^^^^^
	
	gtk_widget_destroy(ref->dialog);
	ref->dialog = NULL;
	
	// FIXME : the dtor is not correctly called???
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	ref->prj->CloseTrajectory();	// ???
}

/*################################################################################################*/

// eof

// LOCAL_I18N.H : some internationalization stuff...

// Copyright (C) 2008 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

// only ".cpp"-files are supposed to include this!!!
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// also make sure that this file is not installed in "PREFIX/include".
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

// here we include and/or define necessary stuff so that gettext()
// and related stuff is correclty found when needed.

// the idea is that gettext() may be also easily disabled by
// modifying just this file and nothing else!!!

/*################################################################################################*/

// enable i18n:
// ^^^^^^^^^^^^

#include "ghemicalconfig.h"	// GETTEXT_PACKAGE is defined here...
#include <glib/gi18n-lib.h>

/*################################################################################################*/

// disable i18n:
// ^^^^^^^^^^^^^

//#define _(String) String

/*################################################################################################*/

// eof


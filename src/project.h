// PROJECT.H : base classes for all graphical user interfaces.

// Copyright (C) 1998 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef PROJECT_H
#define PROJECT_H

#include "ghemicalconfig2.h"

class project;
class dummy_project;

class color_mode;
class color_mode_element;		// atom *
class color_mode_secstruct;		// atom *
class color_mode_hydphob;		// atom *

class transparent_primitive;
class transparent_primitive_data;

class tpd_tri_3c;
class tpd_quad_4c;

class ogl_dummy_object;		// oglappth/ogl_objects.h
class ogl_smart_object;		// oglappth/ogl_objects.h

class custom_camera;		// custom_camera.h
class ogl_light;		// oglappth/ogl_camera.h

#include <ghemical/model.h>
#include <ghemical/geomopt.h>
#include <ghemical/moldyn.h>

#include "oglview_wcl.h"

#include "p1dview_wcl.h"
#include "p2dview_wcl.h"
#include "eldview_wcl.h"
#include "rcpview_wcl.h"
#include "gpcview_wcl.h"

#include <map>
#include <vector>
using namespace std;

#define FILENAME_FILTER	"*.gpr"

/*################################################################################################*/

class jobinfo_GeomOpt
{
	public:
	
	project * prj;
	geomopt_param go;	// this is a class...
	bool show_dialog;
	
	jobinfo_GeomOpt(void) : go(NULL)
	{
		prj = NULL;
		show_dialog = false;
	}
};

class jobinfo_MolDyn
{
	public:
	
	project * prj;
	moldyn_param md;	// this is a class...
	bool show_dialog;
	
	jobinfo_MolDyn(void) : md(NULL)
	{
		prj = NULL;
		show_dialog = false;
	}
};

struct jobinfo_RandomSearch
{
	project * prj;
	
	int cycles;
	int optsteps;
};

/*################################################################################################*/

class custom_transformer_client :
	public ogl_transformer_client
{
	public:
	
	bool tc_local_object;	// is the object affected by transformation?
	
	public:
	
	custom_transformer_client(void);
	~custom_transformer_client(void);
};

/*################################################################################################*/

/**	The "##project" class is an enhanced version of "##model" class, containing
	graphical user interface.
*/

class project :
	public custom_transformer_client,
	public model
{
	protected:
	
	char * project_path;		// may or may not exist...
	char * project_filename;	// should always exist...
	
// call oglview-type views graphics views (by tradition)...
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	vector<oglview_wcl *> graphics_view_vector;
	
// call all other types of views (except project-view) plotting views.
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	vector<base_wcl *> plotting_view_vector;
	
	i32s camera_counter;
	i32s object_counter;
	
/**	A static counter of display list IDs.
	In some targets there can be several GUI's running simultaneously,
	so we must make sure that the display list ID's are unique -> static.
*/
	static iGLu list_counter;
	
	atom * mt_a1;	// measure_tool
	atom * mt_a2;	// measure_tool
	atom * mt_a3;	// measure_tool
	
	readpdb_mdata * importpdb_mdata;	// temporary?!?!?!
	
	friend class gtk_importpdb_dialog;	// importpdb_mdata
	
	public:
	
	vector<ogl_smart_object *> object_vector;
	static ogl_dummy_object * selected_object;
	
	static const char appversion[16];
	static char appdata_path[256];		// also see libghemical_init()...

/**	If background_job_running is "true" it means that a geometry optimization,
	molecular dynamics etc. job is running on the background, and the user possibly
	can access any of the GUI elements. What we want is to block user out from any
	GUI elements that might be able directly or indirectly modify any internal state
	or information of the model object (for example addition or removal of atoms or
	bonds will instantly mess up everything ; sorting atoms/bonds does the same ;
	etc...). However it is good if user can update graphics using zoom/turning tools
	and so on...
*/
	static bool background_job_running;
	
	static color_mode_element cm_element;
	static color_mode_secstruct cm_secstruct;
	static color_mode_hydphob cm_hydphob;
	
	atom * draw_data[2];
	
	public:
	
	project(void);
	virtual ~project(void);
	
	void ClearAll(void);
	
	virtual bool Question(const char *) = 0;
	
	// some file name methods...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^
	
	const char * GetProjectFileNameExtension(void);
	
	void SetProjectPath(const char *);
	void SetProjectFileName(const char *);
	
	void SetDefaultProjectFileName(void);
	
	void ParseProjectFileNameAndPath(const char *);
	
	void GetProjectFileName(char *, int, bool);
	void GetFullProjectFileName(char *, int);

// methods for file I/O : ReadGPR and WriteGPR are friend functions so that it would be easier to "borrow" them elsewhere...
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	friend bool ReadGPR_OLD(project &, istream &, bool, bool = false);		///< this is for the very old version.
	friend bool ReadGPR_v100(project &, istream &, bool, bool = false);		///< this is for the version 1.00.
	friend bool ReadGPR_v110(project &, istream &, bool, bool = false);		///< this is for the version 1.10.
/// This is an input function for the v1.11 ghemical file format.
	friend bool ReadGPR(project &, istream &, bool, bool = false);
	
	friend void WriteGPR_v100(project &, ostream &);	///< this is for the version 1.00.
/// This is an output function for the v1.11 ghemical file format.
	friend void WriteGPR(project &, ostream &);
	
#ifdef ENABLE_OPENBABEL
	
	bool ImportFile(const char * filename, int index = 0);
	bool ExportFile(const char * filename, int index = 0);
	
#endif	// ENABLE_OPENBABEL
	
	// add or remove hydrogen atoms...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	void AddH(void);
	void RemoveH(void);
	
	// some utility functions.
	// ^^^^^^^^^^^^^^^^^^^^^^^

	const char * GetType(void);
	color_mode * GetDefaultColorMode(void);
	
	void SelectAll(void);
	void InvertSelection(void);
	
	void HideSelected(void);
	void ShowSelected(void);
	void LockSelected(void);
	void UnlockSelected(void);
	void DeleteSelected(void);
	
	iGLu GetDisplayListIDs(iGLu);
	void DeleteDisplayLists(iGLu, iGLu);
	
// we need to intercept these atom/bond events from the
// model class in order to keep "project view" up-to-date!
	
	void AddAtom_lg(atom &);	// virtual
	void RemoveAtom(iter_al);	// virtual
	
	void AddBond(bond &);		// virtual
	void RemoveBond(iter_bl);	// virtual
	
	void InvalidateGroups(void);	// virtual
	void UpdateChains(void);	// virtual
	
	// some ogl_object-related methods:
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	i32s IsObject(const ogl_dummy_object *);
	bool SelectObject(const ogl_dummy_object *);
	
	void AddObject(ogl_smart_object *);
	bool RemoveObject(ogl_dummy_object *);
	
	// some camera/window methods:
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	fGL GetDefaultFocus(void);
	
	virtual base_wnd * CreateGraphicsWnd(bool) = 0;
	virtual void DestroyGraphicsWnd(base_wnd *) = 0;
	
	virtual base_wnd * CreatePlot1DWnd(bool) = 0;
	virtual base_wnd * CreatePlot2DWnd(bool) = 0;
	virtual base_wnd * CreateEnergyLevelDiagramWnd(bool) = 0;
	virtual base_wnd * CreateReactionCoordinatePlotWnd(bool) = 0;
	virtual base_wnd * CreateGenericProteinChainWnd(bool) = 0;
	virtual void DestroyPlottingWnd(base_wnd *) = 0;
	
// if custom_camera is NULL, a new one is created ; this is the add cam/new logic.
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	oglview_wcl * AddGraphicsClient(custom_camera *, bool);
	bool RemoveGraphicsClient(oglview_wcl *, bool);
	bool IsThisLastGraphicsClient(oglview_wcl *);
	
// each "plotting" client will create an ogl_camera object of it's own ; none is given.
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	p1dview_wcl * AddPlot1DClient(const char *, const char *, bool);
	p2dview_wcl * AddPlot2DClient(const char *, const char *, const char *, bool);
	eldview_wcl * AddEnergyLevelDiagramClient(bool);
	rcpview_wcl * AddReactionCoordinatePlotClient(const char *, const char *, bool);
	gpcview_wcl * AddGenericProteinChainClient(bool);
	bool RemovePlottingClient(base_wcl *);
	
	void UpdateAllViews(void);
	
	void UpdateAllGraphicsViews(bool = false);	// virtual
	void UpdateAllPlottingViews(bool = false);
	
	void UpdateGraphicsViews(ogl_camera *, bool = false);
	void UpdateGraphicsView(oglview_wcl *, bool = false);
	
/**	Contains some OpenGL initialization commands 
	(color, material, light settings). 
	The OGL selection buffer should also be assigned here!!!
*/
	void InitGL(void);
	
/**	Contains the OpenGL rendering commands that draw the 3D-view.

	Selection is done in a rather crude way by dumping the atom pointers directly to 
	the OGL selection buffer and reading them later there... Works fine as long as 
	both datatypes have the same size (currently it's 32 bits). If we some day need 
	64-bit pointers and have only 32-bit sel-buffer we have to use two names...
*/
	void Render(oglview_wcl *, rmode);

/**	This is just quickly cut into a different function for clarity... We will call this
	multiple times with translation applied if we render some periodic systems...
*/
	void RenderOnce(oglview_wcl *, rmode, bool);
	void RenderObjects(oglview_wcl *);
	
	void BeginClientTransformation(ogl_transformer *);	// virtual
	void EndClientTransformation(ogl_transformer *);	// virtual
	
void DrawEvent(oglview_wcl *, vector<iGLu> &);
void EraseEvent(oglview_wcl *, vector<iGLu> &);
	
void SelectEvent(oglview_wcl *, vector<iGLu> &);
void MeasureEvent(oglview_wcl *, vector<iGLu> &);
	
	// some common operations are implemented here:
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	virtual void CreateProgressDialog(const char *, bool, int, int) = 0;
	virtual void DestroyProgressDialog(void) = 0;
	
	void ProcessCommandString(oglview_wcl *, const char *);
	
	virtual void start_job_GeomOpt(jobinfo_GeomOpt *) = 0;
	virtual void start_job_MolDyn(jobinfo_MolDyn *) = 0;
	virtual void start_job_RandomSearch(jobinfo_RandomSearch *) = 0;
	
	void DoDeleteCurrentObject(void);
	
	void DoSwitchLocalLights(ogl_camera *, bool);
	void DoSwitchGlobalLights(ogl_camera *, bool);
	
	bool TestAtom(atom *, rmode);
	bool TestBond(bond *, rmode);
	
	inline void SetColor(color_mode *, atom *, bool);
	
	void DrawCylinder1(const fGL **, const fGL **, const fGL *);
	
// here are some Do???() functions similar to those in the model class.
// however, the classes here are "interactive" and are directly dependent on graphics.
// so you don't want to use any of these classes in the console version...

	void DoFormula(void);
	
	void DoEnergyPlot1D(i32s, i32s, i32s, i32s, i32s, fGL, fGL, i32s);
	void DoEnergyPlot2D(i32s, i32s, i32s, i32s, i32s, fGL, fGL, i32s, i32s, i32s, i32s, i32s, fGL, fGL, i32s);
	
	void DoTransitionStateSearch(f64, f64);
	void DoStationaryStateSearch(i32s);
};

float measure_len(float *, float *);
float measure_ang(float *, float *, float *);
float measure_tor(float *, float *, float *, float *);

/*################################################################################################*/

/// the purpose of this class is just to make file operations available (ReadGPR() wants project object).

class dummy_project : public project
{
	public:
	
	dummy_project(void);
	~dummy_project(void);
	
	void CreateProgressDialog(const char *, bool, int, int) { }
	void DestroyProgressDialog(void) { }
	
	void start_job_GeomOpt(jobinfo_GeomOpt *) { }
	void start_job_MolDyn(jobinfo_MolDyn *) { }
	void start_job_RandomSearch(jobinfo_RandomSearch *) { }
	
	bool Question(const char *) { return false; }
	
	base_wnd * CreateGraphicsWnd(bool) { return NULL; }
	void DestroyGraphicsWnd(base_wnd *) { }
	
	base_wnd * CreatePlot1DWnd(bool) { return NULL; }
	base_wnd * CreatePlot2DWnd(bool) { return NULL; }
	base_wnd * CreateEnergyLevelDiagramWnd(bool) { return NULL; }
	base_wnd * CreateReactionCoordinatePlotWnd(bool) { return NULL; }
	base_wnd * CreateGenericProteinChainWnd(bool) { return NULL; }
	void DestroyPlottingWnd(base_wnd *) { }
};

/*################################################################################################*/

/// The "##color_mode" class is used to handle the color detemination details.

class color_mode
{
	protected:
	
// this is just an interface definition -> there is no relevant common data?!?!?
// this is just an interface definition -> there is no relevant common data?!?!?
// this is just an interface definition -> there is no relevant common data?!?!?

	public:
	
	color_mode(void) { }
	virtual ~color_mode(void) { }
	
// is this (void *)-method really the only possibility to give a virtual function different
// parameters without the "hiding problem"???????????????????

// should be [void *, i32s CRD_SET, fGL_a4] -> crd-set can be included in coloring??? ALSO NEED TO FIND A BETTER WAY TO INCLUDE PREFS!!!
// should be [void *, i32s CRD_SET, fGL_a4] -> crd-set can be included in coloring??? ALSO NEED TO FIND A BETTER WAY TO INCLUDE PREFS!!!
// should be [void *, i32s CRD_SET, fGL_a4] -> crd-set can be included in coloring??? ALSO NEED TO FIND A BETTER WAY TO INCLUDE PREFS!!!

	virtual void GetColor4(const void *, i32s, fGL *) = 0;
};

class color_mode_element : public color_mode
{
	protected:
	
	public:
	
	color_mode_element(void) : color_mode() { }
	~color_mode_element(void) { }
	
	void GetColor4(const void *, i32s, fGL *);
};

class color_mode_secstruct : public color_mode
{
	protected:
	
	public:
	
	color_mode_secstruct(void) : color_mode() { }
	~color_mode_secstruct(void) { }
	
	void GetColor4(const void *, i32s, fGL *);
};

class color_mode_hydphob : public color_mode
{
	protected:
	
	public:
	
	color_mode_hydphob(void) : color_mode() { }
	~color_mode_hydphob(void) { }
	
	void GetColor4(const void *, i32s, fGL *);
};

/*################################################################################################*/

#endif	// PROJECT_H

// eof

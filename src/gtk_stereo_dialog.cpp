// GTK_STEREO_DIALOG.CPP

// Copyright (C) 2000 Tommi Hassinen, Mike Cruz.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_stereo_dialog.h"

#include "gtk_app.h"
#include "local_i18n.h"

/*################################################################################################*/

gtk_stereo_dialog::gtk_stereo_dialog(gtk_oglview_wnd * p1) :
	gtk_glade_dialog("glade/gtk_stereo_dialog.glade")
{
	wnd = p1;
	scaling = 1.0;
	
	dialog = glade_xml_get_widget(xml, "stereo_dialog");
	if (dialog == NULL)
	{
		cout << _("WARNING : stereo_dialog : glade_xml_get_widget() failed!!!") << endl;
		return;
	}
	
	// connect the handlers...
	
	glade_xml_signal_connect_data(xml, "on_dialog_destroy", (GtkSignalFunc) handler_Destroy, (gpointer) this);
	
	glade_xml_signal_connect_data(xml, "on_hscale_separation_value_changed", (GtkSignalFunc) handler_SepChanged, (gpointer) this);
	glade_xml_signal_connect_data(xml, "on_hscale_displacement_value_changed", (GtkSignalFunc) handler_DispChanged, (gpointer) this);
	
	gtk_widget_show(dialog);	// MODELESS
}

gtk_stereo_dialog::~gtk_stereo_dialog(void)
{
}

void gtk_stereo_dialog::SetScaling(float tmpf)
{
	scaling = tmpf;
}

void gtk_stereo_dialog::CloseDialog(void)
{
	gtk_widget_destroy(dialog);
	dialog = NULL;
}

void gtk_stereo_dialog::handler_Destroy(GtkWidget *, gpointer data)
{
	gtk_stereo_dialog * ref = (gtk_stereo_dialog *) data;
	cout << "DEBUG : handler_Destroy() : ref = " << ref << endl;
	
	// tell others that we are closed!!!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	ref->wnd->my_stereo_dialog = NULL;
	ref->wnd = NULL;
}

void gtk_stereo_dialog::handler_SepChanged(GtkWidget * range, gpointer data)
{
	gtk_stereo_dialog * ref = (gtk_stereo_dialog *) data;
	
	const float value = gtk_range_get_value(GTK_RANGE(range)) * ref->scaling;
	//cout << "DEBUG : gtk_stereo_dialog::handler_SepChanged() " << value << endl;
	
	ref->wnd->GetClient()->GetCam()->relaxed_separation = value;
	gtk_app::GetPrjX()->UpdateGraphicsViews(ref->wnd->GetClient()->GetCam());
}

void gtk_stereo_dialog::handler_DispChanged(GtkWidget * range, gpointer data)
{
	gtk_stereo_dialog * ref = (gtk_stereo_dialog *) data;
	
	const float value = gtk_range_get_value(GTK_RANGE(range)) * ref->scaling;
	//cout << "DEBUG : gtk_stereo_dialog::handler_DispChanged() " << value << endl;
	
	ref->wnd->GetClient()->GetCam()->stereo_displacement = value;
	gtk_app::GetPrjX()->UpdateGraphicsViews(ref->wnd->GetClient()->GetCam());
}

/*################################################################################################*/

// eof

// GTK_OGLVIEW_WND.H : write a short description here...

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ghemicalconfig2.h"

#ifndef GTK_OGLVIEW_WND_H
#define GTK_OGLVIEW_WND_H

class gtk_oglview_wnd;

#include "gtk_wnd.h"
#include "custom_camera.h"
#include "gtk_stereo_dialog.h"

/*################################################################################################*/

class gtk_oglview_wnd :
	public gtk_wnd
{
	private:
	
	static GtkActionEntry entries[];
	static const char * ui_description;
	
	gtk_stereo_dialog * my_stereo_dialog;
	
	guint timer_id;
	
	friend class gtk_stereo_dialog;
	
	public:
	
	gtk_oglview_wnd(bool);
	~gtk_oglview_wnd(void);
	
	bool IsTimerON(void);		// virtual
	
	void SetTimerON(int);		// virtual
	void SetTimerOFF(void);		// virtual
	
	static gboolean TimerHandler(GtkWidget *);
	
// the popup-menu callbacks start here ; the popup-menu callbacks start here
// the popup-menu callbacks start here ; the popup-menu callbacks start here
// the popup-menu callbacks start here ; the popup-menu callbacks start here
	
	static void popup_ProjOrthographic(GtkWidget *, gpointer);	// toggle!!!
	static void popup_ProjPerspective(GtkWidget *, gpointer);	// toggle!!!
	
	static void popup_ProjSterRedBlue(GtkWidget *, gpointer);	// toggle!!!
	static void popup_ProjSterRelaxed(GtkWidget *, gpointer);	// toggle!!!
	
	static void popup_RenderQuickUpdate(GtkWidget *, gpointer);	// toggle!!!
	
	static void popup_ViewsAttachDetach(GtkWidget *, gpointer);
	static void popup_ViewsNewWnd(GtkWidget *, gpointer);
	static void popup_ViewsNewCam(GtkWidget *, gpointer);
	static void popup_ViewsDeleteView(GtkWidget *, gpointer);
	
	static void popup_ViewsPushCRDSet(GtkWidget *, gpointer);
	static void popup_ViewsSuperimpose(GtkWidget *, gpointer);
	
	static void popup_LightsNewLight(GtkWidget *, gpointer);
	static void popup_LightsSwitchLoc(GtkWidget *, gpointer);
	static void popup_LightsSwitchGlob(GtkWidget *, gpointer);
};

/*################################################################################################*/

#endif	// GTK_OGLVIEW_WND_H

// eof

// OGL_SURFACE.H : a class for drawing colored 3D-surfaces.

// Copyright (C) 1998 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef OGL_SURFACE_H
#define OGL_SURFACE_H

//#include "ghemicalconfig2.h"

struct ogl_cs_line;
struct ogl_cs_triangle;

struct ogl_cs_vertex;

struct ogl_cs_param;
class ogl_color_surface;

/*################################################################################################*/


#ifdef WIN32
#include <windows.h>	// need to have this before GL stuff...
#endif	// WIN32

#include <GL/gl.h>

#include <vector>
#include <algorithm>
using namespace std;

//#include "ogl_camera.h"
#include <oglappth/ogl_camera.h>
#include <oglappth/transparent.h>

#include <ghemical/typedef.h>

#include "project.h"

/*################################################################################################*/

struct ogl_cs_line
{
	i32s index[2];
	
	void Arrange(void)
	{
		sort(index, index + 2);
	}
	
	bool operator==(const ogl_cs_line & p1) const
	{
		if (index[0] != p1.index[0]) return false;
		if (index[1] != p1.index[1]) return false;
		return true;
	}
	
	bool operator<(const ogl_cs_line & p1) const
	{
		if (index[0] != p1.index[0]) return index[0] < p1.index[0];
		if (index[1] != p1.index[1]) return index[1] < p1.index[1];
		return false;
	}
};

struct ogl_cs_triangle
{
	i32s index[3];
};

struct ogl_cs_vertex
{
	fGL crd[3]; fGL normal[3];
	fGL color[4]; fGL cvalue; i32s id;
	
	void Render(bool normal_flag)
	{
		glColor4fv(color);
		if (normal_flag) glNormal3fv(normal);
		glVertex3fv(crd);
	}
};

/*################################################################################################*/

struct ogl_cs_param
{
	project * prj;
	const ogl_obj_loc_data * data;	// for color_surface only!!!
	
	iGLu my_glname;			// for color_surface only!!!
	
	bool transparent;
	bool automatic_cv2;
	bool wireframe;
	
	i32s * np; fGL * dim;
	
	engine * ref;
	ValueFunction * vf1;
	ValueFunction * vf2;
	ColorFunction * cf;
	
	fGL svalue;
	fGL cvalue1;
	fGL cvalue2;
	fGL alpha;
	
	fGL toler;
	i32s maxc;
	
	ogl_cs_param * next;
};

class ogl_color_surface
{
	private:
	
	project * prj;
	const ogl_obj_loc_data * data;
	
	iGLu my_glname;
	
	bool transparent;
	bool automatic_cv2;
	bool wireframe;
	
	i32s np[3]; fGL dim[3];
	
	engine * ref;
	ValueFunction * GetSurfaceValue;
	ValueFunction * GetColorValue;
	ColorFunction * GetColor;
	
	fGL svalue;
	fGL cvalue1;
	fGL cvalue2;
	fGL alpha;
	
	fGL tolerance;
	i32s max_cycles;
	
/*################*/
/*################*/
	
	fGL * dist[3];
	vector<bool> grid;
	
	i32s range[3][2];
	vector<ogl_cs_vertex> vdata;
	vector<ogl_cs_triangle> tdata;
	vector<ogl_cs_line> ldata;
	
	oglv3d<GLfloat> xdir;
	
	f64 avrg; i32u acnt;
	
	public:
	
	ogl_color_surface(ogl_cs_param &);
	~ogl_color_surface(void);
	
	void Update(void);
	void Render(void);
	
	protected:
	
	void SetDimension(fGL *);
	void GetCRD(i32s *, fGL *);
	
	void Interpolate(i32s, i32s *, i32s *);
};

/*################################################################################################*/

class ogl_color_surface_object :
	public ogl_smart_object
{
	private:
	
	project * prj; fGL dim[3];
	vector<ogl_color_surface *> cs_vector;
	
	bool transform_in_progress;
	
	engine * copy_of_ref;
	char * object_name;
	
	public:
	
	ogl_color_surface_object(const ogl_object_location &, ogl_cs_param &, const char *);
	~ogl_color_surface_object(void);
	
	engine * GetRef(void) { return copy_of_ref; }
	const char * GetObjectName(void) { return object_name; }	// virtual
	
	void CameraEvent(const ogl_camera &) { }	// virtual
	
	bool BeginTransformation(void);			// virtual
	bool EndTransformation(void);			// virtual
	
	void OrbitObject(const fGL *, const ogl_camera &);		// virtual
	void RotateObject(const fGL *, const ogl_camera &);		// virtual
	
	void TranslateObject(const fGL *, const ogl_obj_loc_data *);	// virtual
	
	void Render(void);	// virtual
	void Update(void);
};

/*################################################################################################*/

#endif	// OGL_SURFACE_H

// eof

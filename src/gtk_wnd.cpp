// GTK_WND.CPP

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_wnd.h"

#include "gtk_app.h"
#include "local_i18n.h"

#include <GL/gl.h>
#include <GL/glu.h>

/*################################################################################################*/

vector<gtk_wnd *> gtk_wnd::iv;

gtk_wnd::gtk_wnd(bool det_flag) :
	base_wnd()
{
	iv.push_back(this);
	
	view_widget = NULL;
	
	detached = NULL;
	
	label_widget = NULL;
	popupmenu = NULL;
	
	GdkGLConfigMode mode = (GdkGLConfigMode) 0;
	GdkGLConfig * glconfig = NULL;
	
	mode = (GdkGLConfigMode) (GDK_GL_MODE_RGB | GDK_GL_MODE_DEPTH | GDK_GL_MODE_DOUBLE);
	glconfig = gdk_gl_config_new_by_mode(mode);		// try a double-buffered visual...
	
	if (glconfig == NULL)
	{
		g_print(_("*** Cannot find the double-buffered visual.\n"));
		g_print(_("*** Trying single-buffered visual.\n"));
		
		mode = (GdkGLConfigMode) (GDK_GL_MODE_RGB | GDK_GL_MODE_DEPTH);
		glconfig = gdk_gl_config_new_by_mode(mode);	// try a single-buffered visual...
		
		if (glconfig == NULL)
		{
			g_print(_("*** No appropriate OpenGL-capable visual found.\n"));
			exit(EXIT_FAILURE);
		}
	}
	
	g_print (_("\nOpenGL visual configurations :\n\n"));
	g_print ("gdk_gl_config_is_rgba (glconfig) = %s\n", gdk_gl_config_is_rgba (glconfig) ? "TRUE" : "FALSE");
	g_print ("gdk_gl_config_is_double_buffered (glconfig) = %s\n", gdk_gl_config_is_double_buffered (glconfig) ? "TRUE" : "FALSE");
	g_print ("gdk_gl_config_is_stereo (glconfig) = %s\n", gdk_gl_config_is_stereo (glconfig) ? "TRUE" : "FALSE");
	g_print ("gdk_gl_config_has_alpha (glconfig) = %s\n", gdk_gl_config_has_alpha (glconfig) ? "TRUE" : "FALSE");
	g_print ("gdk_gl_config_has_depth_buffer (glconfig) = %s\n", gdk_gl_config_has_depth_buffer (glconfig) ? "TRUE" : "FALSE");
	g_print ("gdk_gl_config_has_stencil_buffer (glconfig) = %s\n", gdk_gl_config_has_stencil_buffer (glconfig) ? "TRUE" : "FALSE");
	g_print ("gdk_gl_config_has_accum_buffer (glconfig) = %s\n", gdk_gl_config_has_accum_buffer (glconfig) ? "TRUE" : "FALSE");
	g_print ("\n");
	
	view_widget = gtk_drawing_area_new();
	gtk_widget_set_size_request(view_widget, 100, 100);	// minimum size...
	
	gtk_widget_set_gl_capability(view_widget, glconfig, NULL, TRUE, GDK_GL_RGBA_TYPE);
	
	int events = GDK_EXPOSURE_MASK;
	events |= GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK;
	events |= GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK;
	gtk_widget_set_events(GTK_WIDGET(view_widget), events);
	
	gtk_signal_connect(GTK_OBJECT(view_widget), "expose_event", GTK_SIGNAL_FUNC(ExposeHandler), NULL);
	gtk_signal_connect(GTK_OBJECT(view_widget), "button_press_event", GTK_SIGNAL_FUNC(ButtonHandler), NULL);
	gtk_signal_connect(GTK_OBJECT(view_widget), "button_release_event", GTK_SIGNAL_FUNC(ButtonHandler), NULL);
	gtk_signal_connect(GTK_OBJECT(view_widget), "motion_notify_event", GTK_SIGNAL_FUNC(MotionNotifyHandler), NULL);
	gtk_signal_connect(GTK_OBJECT(view_widget), "configure_event", GTK_SIGNAL_FUNC(ConfigureHandler), NULL);
	
	gtk_signal_connect_after(GTK_OBJECT(view_widget), "realize", GTK_SIGNAL_FUNC(RealizeHandler), NULL);	// after!!!
	
	gtk_widget_show(GTK_WIDGET(view_widget));
	
	if (det_flag)
	{
		detached = gtk_window_new(GTK_WINDOW_TOPLEVEL);
		
		gtk_window_set_default_size(GTK_WINDOW(detached), 500, 300);
		gtk_app::GetAppX()->SetTransientForMainWnd(GTK_WINDOW(detached));
		
		// we could set the window title here, but at this stage
		// we are always unlinked and cannot get the title text...
		
		gtk_container_add(GTK_CONTAINER(detached), view_widget);
		gtk_signal_connect(GTK_OBJECT(detached), "delete_event", GTK_SIGNAL_FUNC(gtk_wnd::DetachedDeleteHandler), NULL);
		
		gtk_widget_show(detached);
	}
}

gtk_wnd::~gtk_wnd(void)
{
	if (detached != NULL)
	{
		gtk_widget_destroy(GTK_WIDGET(detached));
		detached = NULL;
		
	//	gtk_widget_destroy(GTK_WIDGET(view_widget));	// ???crash???
		view_widget = NULL;	// already destroyed as a child widget?
	}
	else
	{
	//	gtk_widget_destroy(GTK_WIDGET(view_widget));	// ???crash???
		view_widget = NULL;	// already destroyed as a child widget?
	}
	
	// remove the record from iv...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	unsigned int i = 0;
	while (i < iv.size())
	{
		if (iv[i] == this) break;
		else i++;
	}
	
	if (i < iv.size())
	{
		iv.erase(iv.begin() + i);
	}
	else
	{
		assertion_failed(__FILE__, __LINE__, "removal from iv failed!");
	}
}

gtk_wnd * gtk_wnd::iv_Find(GtkWidget * vw)
{
	for (unsigned int i = 0;i < iv.size();i++)
	{
		if (iv[i]->view_widget == vw) return iv[i];
	}
	
	return NULL;
}

void gtk_wnd::RealizeHandler(GtkWidget * widget, gpointer)
{
//cout << "DEBUG : gtk_wnd::RealizeHandler()" << endl;
////////////////////////////////////////////////////////////
	
	// realize signal will occur when the widget is created.
	
	gtk_wnd * w = iv_Find(widget);
	if (w == NULL) cout << "DEBUG : gtk_wnd::RealizeHandler() : iv_Find() failed!" << endl;
	else
	{
		w->SetRealized();
		
		// here it would be natural to do the things like these:
		// SetCurrent() + GetClient()->InitGL() + RequestUpdate(false).
		
		// the problem is that usually GetClient() will return NULL at
		// this stage ; if this is the case, then do nothing here and
		// handle the problem elsewhere (see base_wcl::LinkWnd()).
		
		// this is really a timing issue since we do not know at which
		// point the widget is going to be realized...
		
		if (w->GetClient() != NULL && !w->GetInitialized())
		{
			// usually this stuff is skipped...
			// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			
			w->SetInitialized();
			
		//	w->SetCurrent();	no begin/end here...
		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			
			GdkGLContext * glcontext = gtk_widget_get_gl_context(w->view_widget);
			GdkGLDrawable * gldrawable = gtk_widget_get_gl_drawable(w->view_widget);
			
			if (!gdk_gl_drawable_make_current(gldrawable, glcontext))
			{
				g_print("DEBUG : gtk_wnd::RealizeHandler() : gdk_gl_drawable_make_current() failed.\n");
			}
			else
			{
				gdk_gl_drawable_gl_begin(gldrawable, glcontext);
				
				w->GetClient()->InitGL();
				
				gdk_gl_drawable_gl_end(gldrawable);
			}
			
			w->RequestUpdate(false);
		}
	}
	
	// no return value for this event?!?!?!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
}

gint gtk_wnd::ExposeHandler(GtkWidget * widget, GdkEventExpose * event)
{
//cout << "DEBUG : gtk_wnd::ExposeHandler()" << endl;
////////////////////////////////////////////////////////////
	
	// expose_event will occur when the window should be re-drawn.
// IS SOMETHING WRONG IN HERE??? see the comments on configure-handler...
// IS SOMETHING WRONG IN HERE??? see the comments on configure-handler...
// IS SOMETHING WRONG IN HERE??? see the comments on configure-handler...
	
	if (event->count > 0) return TRUE;	// draw only last expose.
	
	gtk_wnd * w = iv_Find(widget);
	if (w == NULL) cout << "DEBUG : gtk_wnd::ButtonHandler() : iv_Find() failed!" << endl;
	else
	{
		if (!w->GetRealized())
		{
			assertion_failed(__FILE__, __LINE__, "NOT REALIZED!");
		}
		
		if (!w->GetInitialized())
		{
			assertion_failed(__FILE__, __LINE__, "NOT INITIALIZED!");
		}
		
		base_wcl * wcl = w->GetClient();
		if (wcl != NULL)
		{
			GdkGLContext * glcontext = gtk_widget_get_gl_context(w->view_widget);
			GdkGLDrawable * gldrawable = gtk_widget_get_gl_drawable(w->view_widget);
			
			if (!gdk_gl_drawable_make_current(gldrawable, glcontext))
			{
				g_print("DEBUG : gtk_wnd::ExposeHandler() : gdk_gl_drawable_make_current() failed.\n");
			}
			else
			{
				gdk_gl_drawable_gl_begin(gldrawable, glcontext);
				
				wcl->UpdateWnd();
				
				if (gdk_gl_drawable_is_double_buffered(gldrawable))
				{
					gdk_gl_drawable_swap_buffers(gldrawable);
				}
				else
				{
					glFlush();
				}
				
				gdk_gl_drawable_gl_end(gldrawable);
			}
		}
	}
	
//	return TRUE;	// which one this should be???
	return FALSE;	// which one this should be??? scribble.c example used this.
}

int button_event_lost_counter = 0;

gint gtk_wnd::ButtonHandler(GtkWidget * widget, GdkEventButton * eb)
{
//cout << "DEBUG : gtk_wnd::ButtonHandler()" << endl;
////////////////////////////////////////////////////////////
	
	// button_press/release_event(s) are triggered by mouse events.
	
	gtk_wnd * w = iv_Find(widget);
	if (w == NULL) cout << "DEBUG : gtk_wnd::ButtonHandler() : iv_Find() failed!" << endl;
	else
	{
		if (!w->GetRealized())
		{
			assertion_failed(__FILE__, __LINE__, "NOT REALIZED!");
		}
		
		mouseinfo::mi_button tmpb;
		i32s tmps1;
		
		switch (eb->button)
		{
			case 1:
			tmpb = mouseinfo::bLeft;
			tmps1 = GDK_BUTTON1_MASK;
			break;
			
			case 3:
			tmpb = mouseinfo::bRight;
			tmps1 = GDK_BUTTON3_MASK;
			break;
			
			default:
			tmpb = mouseinfo::bMiddle;
			tmps1 = GDK_BUTTON2_MASK;
		}
		
		mouseinfo::mi_state tmps2 = (eb->state & tmps1) ? mouseinfo::sUp : mouseinfo::sDown;
		
		if (tmps2 == mouseinfo::sDown)
		{
			if (mouseinfo::button == mouseinfo::bNone)
			{
				if (tmpb == mouseinfo::bRight)
				{
					if (project::background_job_running) return TRUE;
					
					// the popup menu is created here. pointer to the gtk_drawing_area
					// widget is given as "user_data", and it is also passed to the popup
					// hander callback function (instead of the original value).
					
					if (w->popupmenu != NULL)
					{
						gtk_menu_popup(GTK_MENU(w->popupmenu), NULL, NULL, NULL, NULL, eb->button, eb->time);
					}
					
					return TRUE;
				}
				
				mouseinfo::button = tmpb;
				
				mouseinfo::shift_down = (eb->state & GDK_SHIFT_MASK) ? true : false;
				mouseinfo::ctrl_down = (eb->state & GDK_CONTROL_MASK) ? true : false;
				
				mouseinfo::state = mouseinfo::sDown;
				
			//	cout << "DEBUG : button_event_D " << mouseinfo::button << " " << mouseinfo::state << endl;
				w->GetClient()->ButtonEvent((i32s) eb->x, (i32s) eb->y);
				
				// this is for exceptions, see below...
				// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
				button_event_lost_counter = 0;
			}
		}
		else
		{
			if (mouseinfo::button == mouseinfo::bLeft && tmpb != mouseinfo::bLeft) return TRUE;
			if (mouseinfo::button == mouseinfo::bMiddle && tmpb != mouseinfo::bMiddle) return TRUE;
			if (mouseinfo::button == mouseinfo::bRight && tmpb != mouseinfo::bRight) return TRUE;
			
			mouseinfo::state = mouseinfo::sUp;
			
		//	cout << "DEBUG : button_event_U " << mouseinfo::button << " " << mouseinfo::state << endl;
			w->GetClient()->ButtonEvent((i32s) eb->x, (i32s) eb->y);
			
			mouseinfo::button = mouseinfo::bNone;
		}
	}
	
	return TRUE;
}

gint gtk_wnd::MotionNotifyHandler(GtkWidget * widget, GdkEventMotion * event)
{
//cout << "DEBUG : gtk_wnd::MotionNotifyHandler()" << endl;
////////////////////////////////////////////////////////////
	
	// mouse movements will trigger the events here...
	
	int x; int y; GdkModifierType mbstate;
	if (event->is_hint) gdk_window_get_pointer(event->window, & x, & y, & mbstate);
	else { x = (int) event->x; y = (int) event->y; mbstate = (GdkModifierType) event->state; }
	
	// here it is good to check if we have lost a "mouse button up" message.
	// it can happen if a user moves the mouse outside to the graphics window,
	// and then changes the mousebutton state.
	
	// if we think that a mouse button should be down, but GTK+ says it's not,
	// then immediately send a "mouse button down" message...
	
	bool no_buttons_down = !(mbstate & (GDK_BUTTON1_MASK | GDK_BUTTON2_MASK | GDK_BUTTON3_MASK));
	if (no_buttons_down && mouseinfo::button != mouseinfo::bNone)
	{
		button_event_lost_counter++;
		if (button_event_lost_counter > 1)
		{
			gtk_wnd * w = iv_Find(widget);
			if (w == NULL) cout << "DEBUG : gtk_wnd::MotionNotifyHandler() : iv_Find() failed!" << endl;
			else
			{
				cout << "DEBUG : WARNING ; a mouse-button-up event was lost!" << endl;
				
				mouseinfo::state = mouseinfo::sUp;
				
			//	cout << "DEBUG : button_event_U " << mouseinfo::button << " " << mouseinfo::state << endl;
				w->GetClient()->ButtonEvent((i32s) x, (i32s) y);
				
				mouseinfo::button = mouseinfo::bNone;
			}
		}
	}
	
	// the normal operation starts here...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	if (mouseinfo::button != mouseinfo::bNone)
	{
		gtk_wnd * w = iv_Find(widget);
		if (w == NULL) cout << "DEBUG : gtk_wnd::MotionNotifyHandler() : iv_Find() failed!" << endl;
		else
		{
		//	cout << "DEBUG : motion_event " << mouseinfo::button << " " << mouseinfo::state << endl;
			w->GetClient()->MotionEvent(x, y);
		}
	}
	
	return TRUE;
}

gint gtk_wnd::ConfigureHandler(GtkWidget * widget, GdkEventConfigure *)
{
//cout << "DEBUG : gtk_wnd::ConfigureHandler()" << endl;
////////////////////////////////////////////////////////////
	
	// configure_event will occur at each window size change,
	// and when the window is initially created.
	
	gtk_wnd * w = iv_Find(widget);
	if (w != NULL)
	{
		w->SetWidth(widget->allocation.width);
		w->SetHeight(widget->allocation.height);
	}
	else cout << "DEBUG : iv_Find() failed at gtk_wnd::ConfigureHandler()." << endl;
	
	// the screen is NOT always properly updated after a configure-event.
	// sometimes it gets rendered 100% fine, but sometimes only partially
	// or sometimes not at all (leaving a blank screen). it is possible to
	// do w->RequestUpdate(false); here but it does not affect the result...
	
	return TRUE;
}

gint gtk_wnd::DetachedDeleteHandler(GtkWidget *, GdkEvent *)
{
	// when we create detached view windows as GTK_WINDOW_TOPLEVEL, the
	// window will have the "close" button at titlebar. now if the user
	// presses the "close" button, the window-closing sequence will start.
	// we will grab the resulting delete_event here and return TRUE, that
	// will deny the user's request to close the window. the user should
	// use the stardard popup-way of closing the window...
	
	return TRUE;
}

GtkWidget * gtk_wnd::GetViewWidget(void)
{
	return view_widget;
}

bool gtk_wnd::IsDetached(void)
{
	return (detached != NULL);
}

void gtk_wnd::RequestUpdate(bool directly)
{
	if (directly)
	{
		gdk_window_invalidate_rect(view_widget->window, & view_widget->allocation, FALSE);
		gdk_window_process_updates(view_widget->window, FALSE);
	}
	else
	{
		// this is the original ; replaced 20061006 TH
		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		// gtk_widget_queue_draw(view_widget);
		
		gdk_window_invalidate_rect(view_widget->window, & view_widget->allocation, FALSE);
	}
}

void gtk_wnd::RequestResize(int, int)
{
cout << "DEBUG : gtk_wnd::RequestResize() not yet implemented!" << endl;
}

bool gtk_wnd::SetCurrent(void)
{
	GdkGLContext * glcontext = gtk_widget_get_gl_context(view_widget);
	GdkGLDrawable * gldrawable = gtk_widget_get_gl_drawable(view_widget);
	
	if (!gdk_gl_drawable_make_current(gldrawable, glcontext))
	{
		g_print("DEBUG : gtk_wnd::SetCurrent() : gdk_gl_drawable_make_current() failed.\n");
		return false;
	}
	else
	{
		return true;
	}
}

void gtk_wnd::TitleChanged(void)
{
	if (GetClient() == NULL)
	{
		assertion_failed(__FILE__, __LINE__, "wnd is unlinked.");
	}
	
	if (detached != NULL)
	{
		gtk_window_set_title(GTK_WINDOW(detached), GetClient()->GetTitle());
	}
	else
	{
	//GtkWidget * oldlabel = label_widget;			// not needed??? 20061115 TH
		
		label_widget = gtk_label_new(GetClient()->GetTitle());
		gtk_widget_show(label_widget);
		
		gtk_app::GetAppX()->SetTabTitleNB(view_widget, label_widget);
		
	//if (oldlabel != NULL) gtk_widget_destroy(oldlabel);	// not needed??? 20061115 TH
	}
}

/*################################################################################################*/

// eof

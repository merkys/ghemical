// GTK_MOLDYN_DIALOG.CPP

// Copyright (C) 2002 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_moldyn_dialog.h"

#include "local_i18n.h"

#include <gtk/gtk.h>

#include <cstring>
#include <sstream>
#include <iostream>
using namespace std;

/*################################################################################################*/

gtk_moldyn_dialog::gtk_moldyn_dialog(moldyn_param * p1) :
	gtk_glade_dialog("glade/gtk_moldyn_dialog.glade")
{
	param = p1;
	
	dialog = glade_xml_get_widget(xml, "moldyn_dialog");
	if (dialog == NULL)
	{
		cout << _("WARNING : moldyn_dialog : glade_xml_get_widget() failed!!!") << endl;
		return;
	}
	
	// initialize the widgets...
	
	entry_nsteps_h = glade_xml_get_widget(xml, "entry_nsteps_h");
	entry_nsteps_e = glade_xml_get_widget(xml, "entry_nsteps_e");
	entry_nsteps_s = glade_xml_get_widget(xml, "entry_nsteps_s");
	entry_nsteps_c = glade_xml_get_widget(xml, "entry_nsteps_c");
	entry_timestep = glade_xml_get_widget(xml, "entry_timestep");
	
	entry_temperature = glade_xml_get_widget(xml, "entry_temperature");
	entry_T_rt_hc = glade_xml_get_widget(xml, "entry_T_rt_hc");
	entry_T_rt_es = glade_xml_get_widget(xml, "entry_T_rt_es");
	
	entry_pressure = glade_xml_get_widget(xml, "entry_pressure");
	entry_P_rtime = glade_xml_get_widget(xml, "entry_P_rtime");
	entry_P_beta = glade_xml_get_widget(xml, "entry_P_beta");
	
	checkbutton_constant_T = glade_xml_get_widget(xml, "checkbutton_constant_T");
	checkbutton_constant_P = glade_xml_get_widget(xml, "checkbutton_constant_P");
//checkbutton_langevin = glade_xml_get_widget(xml, "checkbutton_langevin");	// REMOVED
	
	entry_trajfile = glade_xml_get_widget(xml, "entry_trajfile");
	button_browse = glade_xml_get_widget(xml, "button_browse");
	file_selector = NULL;
	
	ostringstream str;
	
	str << param->nsteps_h << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_nsteps_h), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->nsteps_e << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_nsteps_e), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->nsteps_s << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_nsteps_s), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->nsteps_c << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_nsteps_c), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->timestep << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_timestep), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->target_T << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_temperature), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->T_rtime_hc << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_T_rt_hc), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->T_rtime_es << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_T_rt_es), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->target_P << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_pressure), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->P_rtime << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_P_rtime), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->P_beta << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_P_beta), str.str().c_str());
	
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbutton_constant_T), (param->constant_T ? TRUE : FALSE));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbutton_constant_P), (param->constant_P ? TRUE : FALSE));
//gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbutton_langevin), (param->langevin ? TRUE : FALSE));	// REMOVED
	
	gtk_entry_set_text(GTK_ENTRY(entry_trajfile), param->filename);
	
	// connect the handlers...
	
	glade_xml_signal_connect_data(xml, "on_dialog_destroy", (GtkSignalFunc) handler_Destroy, (gpointer) this);

	glade_xml_signal_connect_data(xml, "on_button_ok_clicked", (GtkSignalFunc) handler_ButtonOK, (gpointer) this);
	glade_xml_signal_connect_data(xml, "on_button_cancel_clicked", (GtkSignalFunc) handler_ButtonCancel, (gpointer) this);
	
	glade_xml_signal_connect_data(xml, "on_button_browse_clicked", (GtkSignalFunc) handler_ButtonBrowse, (gpointer) this);
	
 	gtk_dialog_run(GTK_DIALOG(dialog));	// MODAL
	gtk_widget_destroy(dialog);		// MODAL
}

gtk_moldyn_dialog::~gtk_moldyn_dialog(void)
{
}

void gtk_moldyn_dialog::handler_Destroy(GtkWidget *, gpointer data)			// not really needed...
{
	gtk_moldyn_dialog * ref = (gtk_moldyn_dialog *) data;
	//cout << "handler_Destroy() : ref = " << ref << endl;
}

void gtk_moldyn_dialog::handler_ButtonOK(GtkWidget *, gpointer data)
{
	gtk_moldyn_dialog * ref = (gtk_moldyn_dialog *) data;
	//cout << "handler_ButtonOK() : ref = " << ref << endl;
	
	// read the user's settings from widgets...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	const gchar * buffer;
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_nsteps_h));
	istringstream istr1a(buffer);
	istr1a >> ref->param->nsteps_h;
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_nsteps_e));
	istringstream istr1b(buffer);
	istr1b >> ref->param->nsteps_e;
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_nsteps_s));
	istringstream istr1c(buffer);
	istr1c >> ref->param->nsteps_s;
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_nsteps_c));
	istringstream istr1d(buffer);
	istr1d >> ref->param->nsteps_c;
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_timestep));
	istringstream istr2(buffer);
	istr2 >> ref->param->timestep;
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_temperature));
	istringstream istr3a(buffer);
	istr3a >> ref->param->target_T;
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_T_rt_hc));
	istringstream istr3b(buffer);
	istr3b >> ref->param->T_rtime_hc;
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_T_rt_es));
	istringstream istr3c(buffer);
	istr3c >> ref->param->T_rtime_es;
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_pressure));
	istringstream istr4a(buffer);
	istr4a >> ref->param->target_P;
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_P_rtime));
	istringstream istr4b(buffer);
	istr4b >> ref->param->P_rtime;
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_P_beta));
	istringstream istr4c(buffer);
	istr4c >> ref->param->P_beta;
	
	ref->param->constant_T = (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ref->checkbutton_constant_T)) == TRUE ? true : false);
	ref->param->constant_P = (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ref->checkbutton_constant_P)) == TRUE ? true : false);
//ref->param->langevin = (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ref->checkbutton_langevin)) == TRUE ? true : false);	// REMOVED
	
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_trajfile));
	strcpy(ref->param->filename, buffer);
	
	ref->param->Confirm();
}

void gtk_moldyn_dialog::handler_ButtonCancel(GtkWidget *, gpointer data)		// not really needed...
{
	gtk_moldyn_dialog * ref = (gtk_moldyn_dialog *) data;
	//cout << "handler_ButtonCancel() : ref = " << ref << endl;
}

void gtk_moldyn_dialog::handler_ButtonBrowse(GtkWidget *, gpointer data)
{
	gtk_moldyn_dialog * ref = (gtk_moldyn_dialog *) data;
	//cout << "handler_ButtonBrowse() : ref = " << ref << endl;
	
	// handle the file selection...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	ref->file_selector = gtk_file_selection_new(_("Select a Trajectory File."));
	g_signal_connect(GTK_FILE_SELECTION(ref->file_selector)->ok_button, "clicked", G_CALLBACK(handler_FileChooser), data);
	
	g_signal_connect_swapped(GTK_FILE_SELECTION(ref->file_selector)->ok_button, "clicked", G_CALLBACK(gtk_widget_destroy), ref->file_selector);
	g_signal_connect_swapped(GTK_FILE_SELECTION(ref->file_selector)->cancel_button, "clicked", G_CALLBACK(gtk_widget_destroy), ref->file_selector);
	
	gtk_dialog_run(GTK_DIALOG(ref->file_selector)); ref->file_selector = NULL;
}

void gtk_moldyn_dialog::handler_FileChooser(GtkWidget *, gpointer data)
{
	gtk_moldyn_dialog * ref = (gtk_moldyn_dialog *) data;
	//cout << "handler_FileChooser() : ref = " << ref << endl;
	
	// save the selected filename...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	const gchar * selected_filename = gtk_file_selection_get_filename(GTK_FILE_SELECTION(ref->file_selector));
	gtk_entry_set_text(GTK_ENTRY(ref->entry_trajfile), selected_filename);
}

/*################################################################################################*/

// eof

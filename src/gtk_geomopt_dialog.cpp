// GTK_GEOMOPT_DIALOG.CPP

// Copyright (C) 2002 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_geomopt_dialog.h"

#include "local_i18n.h"

#include <gtk/gtk.h>

#include <sstream>
#include <iostream>
using namespace std;

/*################################################################################################*/

gtk_geomopt_dialog::gtk_geomopt_dialog(geomopt_param * p1) :
	gtk_glade_dialog("glade/gtk_geomopt_dialog.glade")
{
	param = p1;
	
	dialog = glade_xml_get_widget(xml, "geomopt_dialog");
	if (dialog == NULL)
	{
		cout << _("WARNING : geomopt_dialog : glade_xml_get_widget() failed!!!") << endl;
		return;
	}
	
	// initialize the widgets...
	
	entry_nsteps = glade_xml_get_widget(xml, "entry_nsteps");
	entry_grad = glade_xml_get_widget(xml, "entry_grad");
	entry_delta_e = glade_xml_get_widget(xml, "entry_delta_e");
	
	ostringstream str;
	
	str << param->treshold_nsteps << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_nsteps), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->treshold_grad << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_grad), str.str().c_str());
	
	str.str("");	// blank it...
	
	str << param->treshold_delta_e << ends;
	gtk_entry_set_text(GTK_ENTRY(entry_delta_e), str.str().c_str());
	
	// connect the handlers...
	
	glade_xml_signal_connect_data(xml, "on_dialog_destroy", (GtkSignalFunc) handler_Destroy, (gpointer) this);
	
	glade_xml_signal_connect_data(xml, "on_button_ok_clicked", (GtkSignalFunc) handler_ButtonOK, (gpointer) this);
	glade_xml_signal_connect_data(xml, "on_button_cancel_clicked", (GtkSignalFunc) handler_ButtonCancel, (gpointer) this);
	
	gtk_dialog_run(GTK_DIALOG(dialog));	// MODAL
	gtk_widget_destroy(dialog);		// MODAL
}

gtk_geomopt_dialog::~gtk_geomopt_dialog(void)
{
}

void gtk_geomopt_dialog::handler_Destroy(GtkWidget *, gpointer data)		// not really needed...
{
	gtk_geomopt_dialog * ref = (gtk_geomopt_dialog *) data;
	//cout << "handler_Destroy() : ref = " << ref << endl;
}

void gtk_geomopt_dialog::handler_ButtonOK(GtkWidget *, gpointer data)
{
	gtk_geomopt_dialog * ref = (gtk_geomopt_dialog *) data;
	//cout << "handler_ButtonOK() : ref = " << ref << endl;
	
	// read the user's settings from widgets...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	GtkWidget * cb_nsteps = glade_xml_get_widget(ref->xml, "checkbutton_nsteps");
	GtkWidget * cb_grad = glade_xml_get_widget(ref->xml, "checkbutton_grad");
	GtkWidget * cb_delta_e = glade_xml_get_widget(ref->xml, "checkbutton_delta_e");
	
	const gchar * buffer;
	
	ref->param->enable_nsteps = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_nsteps));
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_nsteps));
	istringstream istr1(buffer);
	istr1 >> ref->param->treshold_nsteps;
	
	ref->param->enable_grad = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_grad));
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_grad));
	istringstream istr2(buffer);
	istr2 >> ref->param->treshold_grad;
	
	ref->param->enable_delta_e = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb_delta_e));
	buffer = gtk_entry_get_text(GTK_ENTRY(ref->entry_delta_e));
	istringstream istr3(buffer);
	istr3 >> ref->param->treshold_delta_e;
	
	if (!ref->param->enable_nsteps && !ref->param->enable_grad && !ref->param->enable_delta_e)
	{
		ref->param->enable_nsteps = true;
		cout << "WARNING: all termination tests were disabled!!! the nsteps test is forced." << endl;
	}
	
	ref->param->Confirm();
}

void gtk_geomopt_dialog::handler_ButtonCancel(GtkWidget *, gpointer data)		// not really needed...
{
	gtk_geomopt_dialog * ref = (gtk_geomopt_dialog *) data;
	//cout << "handler_ButtonCancel() : ref = " << ref << endl;
}

/*################################################################################################*/

// eof

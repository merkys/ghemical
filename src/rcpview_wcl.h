// RCPVIEW_WCL.H : write a short description here...

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ghemicalconfig2.h"

#ifndef RCPVIEW_WCL_H
#define RCPVIEW_WCL_H

#include "p1dview_wcl.h"

//#include <ghemical/typedef.h>

/*################################################################################################*/

class rcpview_wcl :
	public p1dview_wcl
{
	protected:
	
	bool ready;
	
	vector<i32u> patoms;
	vector<bond *> rbonds;
	vector<bond *> pbonds;
	
	fGL r_treshold;
	fGL p_treshold;
	
	public:
	
	rcpview_wcl(const char *, const char *);
	virtual ~rcpview_wcl(void);
	
	void AddPAtom(i32u p1) { patoms.push_back(p1); }
	void AddRBond(bond * p1) { rbonds.push_back(p1); }
	void AddPBond(bond * p1) { pbonds.push_back(p1); }
	
	void Finalize(void);		// virtual
	
	void ButtonEvent(int, int);	// virtual
	void MotionEvent(int, int);	// virtual
};

/*################################################################################################*/

#endif	// RCPVIEW_WCL_H

// eof

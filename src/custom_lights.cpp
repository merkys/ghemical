// CUSTOM_LIGHTS.CPP

// Copyright (C) 2006 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "custom_lights.h"	// config.h is here -> we get ENABLE-macros here...

#include <sstream>
using namespace std;

#include "appdefine.h"

#include <oglappth/base_app.h>

/*################################################################################################*/

#define SHADE_ANGLE (22.5 / 180.0 * M_PI)

rendered_spot_light::rendered_spot_light(const ogl_object_location & p1, const ogl_light_components & p2, GLfloat p3, GLfloat p4) :
	ogl_spot_light(p1, p2, p3, p4)
{
	my_glname = base_app::GetAppB()->RegisterGLName((ogl_dummy_object *) this);
}

rendered_spot_light::~rendered_spot_light(void)
{
	// todo : unregister my_glname!!!
}

void rendered_spot_light::Render(void)
{
	GLfloat r1 = tan(SHADE_ANGLE) * size;
	GLfloat tmp1 = 1.0 / (sin(SHADE_ANGLE) + 1.0);
	GLfloat tmp2 = 1.2 * size;
	
	GLfloat r2 = (1.0 - tmp1) * tmp2;
	GLfloat r3 = tmp1 * tmp2;
	
	glPushName(GLNAME_LIGHT);
	glPushName(my_glname);
	
	glPushMatrix(); SetModelView();
	
	if (cutoff < 180.0)
	{
		glPushMatrix();
		glTranslatef(0.0, 0.0, -r3);
		glBegin(GL_TRIANGLES);
		
		for (int n1 = 0;n1 < 8;n1++)
		{
			if (n1 & 1) glColor3fv(shade1);
			else glColor3fv(shade2);
			
			GLfloat ang1 = M_PI * (GLfloat) n1 / 4.0;
			GLfloat ang2 = M_PI * (GLfloat) (n1 + 1) / 4.0;
			
			glVertex3f(r1 * cos(ang1), r1 * sin(ang1), size);
			glVertex3f(r1 * cos(ang2), r1 * sin(ang2), size);
			glVertex3f(0.0, 0.0, 0.0);
		}
		
		glEnd();	// GL_TRIANGLES
		glPopMatrix();
	}
	
	if (glIsEnabled((GLenum) number)) glColor3fv(bulb_on);
	else glColor3fv(bulb_off);
	
	GLUquadricObj * qo = gluNewQuadric();
	gluQuadricDrawStyle(qo, (GLenum) GLU_FILL);
	gluSphere(qo, 0.95 * r2, 8, 4);
	gluDeleteQuadric(qo);
	
	glPopMatrix();
	
	glPopName();
	glPopName();
}

/*################################################################################################*/

// eof

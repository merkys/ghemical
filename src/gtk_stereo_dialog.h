// GTK_STEREO_DIALOG.H : write a short description here...

// Copyright (C) 2000 Tommi Hassinen, Mike Cruz.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef GTK_STEREO_DIALOG_H
#define GTK_STEREO_DIALOG_H

class gtk_stereo_dialog;

//#include "ghemicalconfig2.h"

#include "gtk_glade_dialog.h"
#include "gtk_oglview_wnd.h"

/*################################################################################################*/

class gtk_stereo_dialog : public gtk_glade_dialog
{
	protected:
	
	gtk_oglview_wnd * wnd;
	float scaling;
	
	GtkWidget * dialog;
	
	public:
	
	gtk_stereo_dialog(gtk_oglview_wnd *);
	~gtk_stereo_dialog(void);
	
	void SetScaling(float);
	void CloseDialog(void);
	
	static void handler_Destroy(GtkWidget *, gpointer);
	
	static void handler_SepChanged(GtkWidget *, gpointer);
	static void handler_DispChanged(GtkWidget *, gpointer);
};

/*################################################################################################*/

#endif	// GTK_STEREO_DIALOG_H

// eof

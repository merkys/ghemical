// CUSTOM_APP.H : write a short description here.

// Copyright (C) 2006 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

//#include "config.h"

#ifndef CUSTOM_APP_H
#define CUSTOM_APP_H

class custom_app;

class ogl_camera;	// liboglappth/ogl_camera.h
class color_mode;	// libghemical/model.h

#include <oglappth/base_app.h>

#include "project.h"
#include "oglview_wcl.h"

#include <vector>
using namespace std;

/*################################################################################################*/

class custom_app :
	public base_app
{
	public:
	
	enum mtool
	{
		mtDraw,
		mtErase,
		mtSelect,
		mtZoom,
		mtClipping,
		mtTranslateXY,
		mtTranslateZ,
		mtOrbitXY,
		mtOrbitZ,
		mtRotateXY,
		mtRotateZ,
		mtMeasure
	};
	
	enum select_mode { smAtom, smResidue, smChain, smMolecule };
	
	protected:
	
	static project * prj;	// SetNewProject() must access this...
	
	static mtool current_mouse_tool;
	static select_mode current_select_mode;
	
// friends...
// ^^^^^^^^^^
	friend class project;
	
	friend class gtk_project;
	
	friend class w32_project;
	friend class w32_tb_button;
	
	public:
	
	custom_app(void);
	virtual ~custom_app(void);
	
	protected:
	
	virtual void SetNewProject(void);
	
	public:
	
	static custom_app * GetAppC(void);
	static project * GetPrj(void);
	
	static mtool GetCurrentMouseTool(void);
	static select_mode GetCurrentSelectMode(void);
	
	virtual void Message(const char *) = 0;
	virtual void WarningMessage(const char *) = 0;
	virtual void ErrorMessage(const char *) = 0;
	
	virtual bool Question(const char *) = 0;
	virtual void PrintToLog(const char *) = 0;
	
	// camera/light-related methods (shared with base_app):
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	void AddCamera(ogl_camera *);			// virtual
	bool RemoveCamera(ogl_camera *);		// virtual
	
	bool AddGlobalLight(ogl_light *);		// virtual
	bool AddLocalLight(ogl_light *, ogl_camera *);	// virtual
	bool RemoveLight(ogl_dummy_object *);		// virtual
	
	bool SelectLight(const ogl_dummy_object *);
	
	// here is the former project_view stuff...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	virtual void UpdateAllWindowTitles(void) = 0;
	
	protected:
	
	virtual void CameraAdded(custom_camera *) = 0;
	virtual void CameraRemoved(custom_camera *) = 0;
	
	virtual void LightAdded(ogl_light *) = 0;
	virtual void LightRemoved(ogl_light *) = 0;
	
	virtual void GraphicsClientAdded(oglview_wcl *) = 0;
	virtual void GraphicsClientRemoved(oglview_wcl *) = 0;
	
	virtual void PlottingClientAdded(base_wcl *) = 0;
	virtual void PlottingClientRemoved(base_wcl *) = 0;
	
	virtual void ObjectAdded(ogl_smart_object *) = 0;
	virtual void ObjectRemoved(ogl_smart_object *) = 0;
	
	virtual void BuildChainsView() = 0;
	virtual void ClearChainsView() = 0;
	
	virtual void AtomAdded(atom *) = 0;
	virtual void AtomUpdateItem(atom *) = 0;	///< updates the properties of an existing object.
	virtual void AtomRemoved(atom *) = 0;
	
	virtual void BondAdded(bond *) = 0;
	virtual void BondUpdateItem(bond *) = 0;	///< updates the properties of an existing object.
	virtual void BondRemoved(bond *) = 0;
};

/*################################################################################################*/

#endif	// CUSTOM_APP_H

// eof

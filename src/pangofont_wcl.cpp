// PANGOFONT_WCL.CPP

// Copyright (C) 2008 Tommi Hassinen, Naosumi Yasufuku.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "pangofont_wcl.h"

#include "local_i18n.h"
#include <ghemical/notice.h>

#include <gtk/gtkgl.h>

#include <cstring>
#include <cstdlib>
using namespace std;

/*################################################################################################*/

pangofont_wcl::pangofont_wcl(ogl_camera * cam) :
	base_wcl(cam)
{
	font_string = NULL;
	font_height = 0;
	
	font_list_base = 0;
}

pangofont_wcl::~pangofont_wcl(void)
{
	if (font_string != NULL)
	{
		g_free(font_string);
		font_string = NULL;
	}
}

void pangofont_wcl::ogl_InitPangoFont(const gchar * fs)
{
	if (!fs)
	{
		assertion_failed(__FILE__, __LINE__, "bad font string.");
	}
	
	if (font_string != NULL)
	{
		cout << "WARNING : pangofont_wcl::ogl_InitPangoFont() is already called." << endl;
		return;
	}
	
	font_string = g_strdup(fs);
	
	// generate font display lists.
	
	font_list_base = glGenLists(128);
	
	PangoFontDescription * font_desc = pango_font_description_from_string(font_string);
	PangoFont * font = gdk_gl_font_use_pango_font(font_desc, 0, 128, font_list_base);
	if (font == NULL)
	{
		g_print(_("*** ERROR : Can't load font '%s'\n"), font_string);
		exit(EXIT_FAILURE);
	}
	
	PangoFontMetrics * font_metrics = pango_font_get_metrics(font, NULL);
	
	font_height = pango_font_metrics_get_ascent(font_metrics) + pango_font_metrics_get_descent(font_metrics);
	font_height = PANGO_PIXELS(font_height);
	
	pango_font_description_free(font_desc);
	pango_font_metrics_unref(font_metrics);
}

int pangofont_wcl::ogl_GetStringWidth(const char * str)
{
	int width = 0;
	
	unsigned int count = 0;
	while (count < strlen(str))
	{
		width += font_height / 2;	// how to do this correctly???
		count++;
	}
	
	return width;
}

void pangofont_wcl::ogl_WriteString2D(const char * str, GLfloat x, GLfloat y)
{
	glPushMatrix();
	glLoadIdentity();
	
	glMatrixMode(GL_PROJECTION);
	glPushMatrix(); glLoadIdentity();
	gluOrtho2D(0, GetWnd()->GetWidth(), 0, GetWnd()->GetHeight());
	
	ogl_WriteString3D(str, x, y, 0.0);
	
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	
	glPopMatrix();
}

void pangofont_wcl::ogl_WriteString3D(const char * str, GLfloat x, GLfloat y, GLfloat z)
{
	glDisable(GL_DEPTH_TEST);
	
	glRasterPos3f(x, y, z);
	
	unsigned int count = 0;
	while (count < strlen(str))
	{
		glCallList(font_list_base + str[count++]);
	}
	
	glEnable(GL_DEPTH_TEST);
}

/*################################################################################################*/

// eof

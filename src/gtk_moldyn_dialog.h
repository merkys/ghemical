// GTK_MOLDYN_DIALOG.H : write a short description here...

// Copyright (C) 2002 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef GTK_MOLDYN_DIALOG_H
#define GTK_MOLDYN_DIALOG_H

//#include "ghemicalconfig2.h"

#include "gtk_glade_dialog.h"

#include <ghemical/moldyn.h>

/*################################################################################################*/

class gtk_moldyn_dialog : public gtk_glade_dialog
{
	protected:
	
	moldyn_param * param;
	
	GtkWidget * dialog;
	
	GtkWidget * entry_nsteps_h;
	GtkWidget * entry_nsteps_e;
	GtkWidget * entry_nsteps_s;
	GtkWidget * entry_nsteps_c;
	GtkWidget * entry_timestep;
	
	GtkWidget * entry_temperature;
	GtkWidget * entry_T_rt_hc;
	GtkWidget * entry_T_rt_es;
	
	GtkWidget * entry_pressure;
	GtkWidget * entry_P_rtime;
	GtkWidget * entry_P_beta;
	
	GtkWidget * checkbutton_constant_T;
	GtkWidget * checkbutton_constant_P;
	//GtkWidget * checkbutton_langevin;	// removed 2008-07-18
	
	GtkWidget * entry_trajfile;
	GtkWidget * button_browse;
	GtkWidget * file_selector;
	
	public:
	
	gtk_moldyn_dialog(moldyn_param *);
	~gtk_moldyn_dialog(void);

	static void handler_Destroy(GtkWidget *, gpointer);
	
	static void handler_ButtonOK(GtkWidget *, gpointer);
	static void handler_ButtonCancel(GtkWidget *, gpointer);
	
	static void handler_ButtonBrowse(GtkWidget *, gpointer);
	static void handler_FileChooser(GtkWidget *, gpointer);
};

/*################################################################################################*/

#endif	// GTK_MOLDYN_DIALOG_H

// eof

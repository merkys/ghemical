// GTK_PROJECT.CPP

// Copyright (C) 1998 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_project.h"	// config.h is here -> we get ENABLE-macros here...

#include "project.h"

#include "appdefine.h"

#include <ghemical/geomopt.h>
#include <ghemical/moldyn.h>

#include <ghemical/eng1_qm.h>
#include <ghemical/eng1_mm.h>
#include <ghemical/eng1_sf.h>

#include "color.h"

#include "gtk_oglview_wnd.h"

#include "gtk_p1dview_wnd.h"
#include "gtk_p2dview_wnd.h"
#include "gtk_eldview_wnd.h"
#include "gtk_rcpview_wnd.h"
#include "gtk_gpcview_wnd.h"

#include "ogl_plane.h"
#include "ogl_surface.h"
#include "ogl_ribbon.h"

#include "gtk_file_import_dialog.h"
#include "gtk_file_export_dialog.h"

#include "gtk_geomopt_dialog.h"
#include "gtk_moldyn_dialog.h"

#include "gtk_setup_dialog.h"
#include "gtk_progress_dialog.h"

#include "local_i18n.h"

#include <glade/glade.h>

#include <sstream>
using namespace std;

/*################################################################################################*/

gtk_project::gtk_project(void) :
	project()
{
	pd = NULL;
}

void gtk_project::DoSafeStart(void)
{
	
// this is effectively the ctor of this class.
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// moved it here so that the objects can be safely constructed and the
// pointer obtained before it is actually used anywhere. 20070816 TH
	
	AddGraphicsClient(NULL, false);
	PrintToLog(_("A new project created.\n"));
}

gtk_project::~gtk_project(void)
{
	if (pd != NULL) cout << "DEBUG : MEM-LEAK pd" << endl;
}

oglview_wcl * gtk_project::GetClient(GtkWidget * widget)
{
	gtk_wnd * wnd = gtk_wnd::iv_Find(widget);
	if (wnd == NULL) cout << "DEBUG : gtk_project::GetClient() failed!" << endl;
	else
	{
		base_wcl * wcl1 = wnd->GetClient();
		if (wcl1 == NULL) cout << "DEBUG : NULL wcl1" << endl;
		
		oglview_wcl * wcl2 = dynamic_cast<oglview_wcl *>(wcl1);
		if (wcl2 == NULL) cout << "DEBUG : NULL wcl2" << endl;
		
		return wcl2;
	}
	
	return NULL;
}

#ifdef ENABLE_THREADS

void gtk_project::ThreadLock(void)
{
	// this is a "technical" thread lock needed in GTK/GDK.
	
	// BUT in addition to this we need to make sure that the
	// user interface is not accessed during any thread processing!
	
	// this means that user may not access menus or mouse tools during
	// multithreaded computations ; see project::background_job_running
	// for more about this...
	
	gdk_threads_enter();
}

void gtk_project::ThreadUnlock(void)
{
	gdk_threads_leave();
}

#else	// ENABLE_THREADS

void gtk_project::NoThreadsIterate(void)
{
	while (gtk_events_pending()) gtk_main_iteration();
}

#endif	// ENABLE_THREADS

bool gtk_project::SetProgress(double progress, double * graphdata)
{
	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pd->progressbar_job), progress);
	
	if (graphdata != NULL)
	{
		int f = (pd->g_fill % pd->g_sz);
		for (int i = 0;i < pd->g_n;i++)
		{
			pd->g_data[i * pd->g_sz + f] = graphdata[i];
		}
		
		pd->g_fill++;
		
		gtk_widget_queue_draw_area(pd->drawingarea_job, 0, 0, pd->da_w, pd->da_h);
	}
	
	return pd->cancel;
}

void gtk_project::CreateProgressDialog(const char * title, bool show_pbar, int graphs_n, int graphs_s)
{
	pd = new gtk_progress_dialog(title, show_pbar, graphs_n, graphs_s);
}

void gtk_project::DestroyProgressDialog(void)
{
	gtk_widget_destroy(pd->dialog);
	delete pd; pd = NULL;
}

void gtk_project::start_job_GeomOpt(jobinfo_GeomOpt * ji)
{
	if (ji->show_dialog)
	{
		new gtk_geomopt_dialog(& ji->go);	// the object will call delete itself...
		cout << "DEBUG : now returning from gtk_project::GeomOptGetParam()." << endl;
		
		// the above dialog is (AND IT MUST BE) a modal one; we will return
		// from the function only after the dialog is closed.
	}
	
	if (!ji->go.GetConfirm()) return;
	
#ifdef ENABLE_THREADS
	
	GThread * t = g_thread_create(process_job_GeomOpt, (gpointer) ji, FALSE, NULL);
	if (t == NULL) ErrorMessage(_("Thread creation failed : GeomOpt"));
	
#else	// ENABLE_THREADS
	
	process_job_GeomOpt((gpointer) ji);
	
#endif	// ENABLE_THREADS
}

gpointer gtk_project::process_job_GeomOpt(gpointer p)
{
	project::background_job_running = true;
	jobinfo_GeomOpt * ji = (jobinfo_GeomOpt *) p;
	
	ji->prj->ThreadLock();
	ji->prj->CreateProgressDialog(_("Geometry Optimization"), true, 1, 20);
	ji->prj->ThreadUnlock();
	
#ifdef ENABLE_THREADS
	const bool updt = false;
#else	// ENABLE_THREADS
	const bool updt = true;
#endif	// ENABLE_THREADS
	
	ji->prj->DoGeomOpt(ji->go, updt);
	
	ji->prj->ThreadLock();
	ji->prj->DestroyProgressDialog();
	ji->prj->ThreadUnlock();
	
	project::background_job_running = false;
	return NULL;
}

void gtk_project::start_job_MolDyn(jobinfo_MolDyn * ji)
{
	if (ji->show_dialog)
	{
		new gtk_moldyn_dialog(& ji->md);		// the object will call delete itself...
		cout << "DEBUG : now returning from gtk_project::MolDynGetParam()." << endl;
		
		// the above dialog is (AND IT MUST BE) a modal one; we will return
		// from the function only after the dialog is closed.
	}
	
	if (!ji->md.GetConfirm()) return;
	
#ifdef ENABLE_THREADS
	
	GThread * t = g_thread_create(process_job_MolDyn, (gpointer) ji, FALSE, NULL);
	if (t == NULL) ErrorMessage(_("Thread creation failed : MolDyn"));
	
#else	// ENABLE_THREADS
	
	process_job_MolDyn((gpointer) ji);
	
#endif	// ENABLE_THREADS
}

gpointer gtk_project::process_job_MolDyn(gpointer p)
{
	project::background_job_running = true;
	jobinfo_MolDyn * ji = (jobinfo_MolDyn *) p;
	
	ji->prj->ThreadLock();
	ji->prj->CreateProgressDialog(_("Molecular Dynamics"), true, NOT_DEFINED, NOT_DEFINED);
	ji->prj->ThreadUnlock();

#ifdef ENABLE_THREADS
	const bool updt = false;
#else	// ENABLE_THREADS
	const bool updt = true;
#endif	// ENABLE_THREADS
	
	ji->prj->DoMolDyn(ji->md, updt);
	
	ji->prj->ThreadLock();
	ji->prj->DestroyProgressDialog();
	ji->prj->ThreadUnlock();
	
	project::background_job_running = false;
	return NULL;
}

void gtk_project::start_job_RandomSearch(jobinfo_RandomSearch * ji)
{
#ifdef ENABLE_THREADS
	
	GThread * t = g_thread_create(process_job_RandomSearch, (gpointer) ji, FALSE, NULL);
	if (t == NULL) ErrorMessage(_("Thread creation failed : RandomSearch"));
	
#else	// ENABLE_THREADS
	
	process_job_RandomSearch((gpointer) ji);
	
#endif	// ENABLE_THREADS
}

gpointer gtk_project::process_job_RandomSearch(gpointer p)
{
	project::background_job_running = true;
	jobinfo_RandomSearch * ji = (jobinfo_RandomSearch *) p;
	
	ji->prj->ThreadLock();
	ji->prj->CreateProgressDialog(_("Random Search"), true, NOT_DEFINED, NOT_DEFINED);
	ji->prj->ThreadUnlock();
	
#ifdef ENABLE_THREADS
	const bool updt = false;
#else	// ENABLE_THREADS
	const bool updt = true;
#endif	// ENABLE_THREADS
	
	ji->prj->DoRandomSearch(ji->cycles, ji->optsteps, updt);
	
	ji->prj->ThreadLock();
	ji->prj->DestroyProgressDialog();
	ji->prj->ThreadUnlock();
	
	project::background_job_running = false;
	return NULL;
}

// the other computation tasks are not yet multithreaded, but the above template can be used...
// the other computation tasks are not yet multithreaded, but the above template can be used...
// the other computation tasks are not yet multithreaded, but the above template can be used...

base_wnd * gtk_project::CreateGraphicsWnd(bool detached)
{
//cout << "gtk_project::CreateGraphicsWnd()" << endl;
	gtk_oglview_wnd * w = new gtk_oglview_wnd(detached);
	
	// since a newly created window is always unlinked,
	// also w->label_widget is always NULL...
	
	if (!detached)
	{
		gtk_app::GetAppX()->AddTabToNB(w->view_widget, w->label_widget);
	}
	
	return w;
}

void gtk_project::DestroyGraphicsWnd(base_wnd * wB)
{
//cout << "gtk_project::DestroyGraphicsWnd()" << endl;
	gtk_oglview_wnd * wX = dynamic_cast<gtk_oglview_wnd *>(wB);
	
// this really seems to be redundant since the plotting views are destoyed the same way...
// this really seems to be redundant since the plotting views are destoyed the same way...
// this really seems to be redundant since the plotting views are destoyed the same way...
	
	if (!wX)
	{
		assertion_failed(__FILE__, __LINE__, "bad wnd!");
	}
	else
	{
		if (!wX->IsDetached())
		{
			gtk_app::GetAppX()->RemoveTabFromNB(wX->view_widget);
		}
		
		delete wB;
	}
}

base_wnd * gtk_project::CreatePlot1DWnd(bool detached)
{
//cout << "gtk_project::CreatePlot1DWnd()" << endl;
	gtk_p1dview_wnd * w = new gtk_p1dview_wnd(detached);
	
	if (!detached)
	{
		gtk_app::GetAppX()->AddTabToNB(w->view_widget, w->label_widget);
	}
	
	return w;
}

base_wnd * gtk_project::CreatePlot2DWnd(bool detached)
{
//cout << "gtk_project::CreatePlot2DWnd()" << endl;
	gtk_p2dview_wnd * w = new gtk_p2dview_wnd(detached);
	
	if (!detached)
	{
		gtk_app::GetAppX()->AddTabToNB(w->view_widget, w->label_widget);
	}
	
	return w;
}

base_wnd * gtk_project::CreateEnergyLevelDiagramWnd(bool detached)
{
//cout << "gtk_project::CreateEnergyLevelDiagramWnd()" << endl;
	gtk_eldview_wnd * w = new gtk_eldview_wnd(detached);
	
	if (!detached)
	{
		gtk_app::GetAppX()->AddTabToNB(w->view_widget, w->label_widget);
	}
	
	return w;
}

base_wnd * gtk_project::CreateReactionCoordinatePlotWnd(bool detached)
{
//cout << "gtk_project::CreateReactionCoordinatePlotWnd()" << endl;
	gtk_rcpview_wnd * w = new gtk_rcpview_wnd(detached);
	
	if (!detached)
	{
		gtk_app::GetAppX()->AddTabToNB(w->view_widget, w->label_widget);
	}
	
	return w;
}

base_wnd * gtk_project::CreateGenericProteinChainWnd(bool detached)
{
//cout << "gtk_project::CreateGenericProteinChainWnd()" << endl;
	gtk_gpcview_wnd * w = new gtk_gpcview_wnd(detached);
	
	if (!detached)
	{
		gtk_app::GetAppX()->AddTabToNB(w->view_widget, w->label_widget);
	}
	
	return w;
}

void gtk_project::DestroyPlottingWnd(base_wnd * wB)
{
//cout << "gtk_project::DestroyPlottingWnd()" << endl;
	gtk_wnd * wX = dynamic_cast<gtk_wnd *>(wB);
	
	if (!wX)
	{
		assertion_failed(__FILE__, __LINE__, "bad wnd!");
	}
	else
	{
		if (!wX->IsDetached())
		{
			gtk_app::GetAppX()->RemoveTabFromNB(wX->view_widget);
		}
		
		delete wB;
	}
}

void gtk_project::Message(const char * msg)
{
	gtk_app::sMessage(msg);
}

void gtk_project::WarningMessage(const char * msg)
{
	gtk_app::sWarningMessage(msg);
}

void gtk_project::ErrorMessage(const char * msg)
{
	gtk_app::sErrorMessage(msg);
}

bool gtk_project::Question(const char * msg)
{
	return gtk_app::sQuestion(msg);
}

void gtk_project::PrintToLog(const char * msg)
{
	gtk_app::sPrintToLog(msg);
}

// the popup-menu callbacks start here ; the popup-menu callbacks start here
// the popup-menu callbacks start here ; the popup-menu callbacks start here
// the popup-menu callbacks start here ; the popup-menu callbacks start here

void gtk_project::popup_FileImport(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	
	// will call delete itself...
	if (prj) new gtk_file_import_dialog(prj);
}

void gtk_project::popup_FileExport(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	
	// will call delete itself...
	if (prj) new gtk_file_export_dialog(prj);
}

void gtk_project::popup_FileExportGraphics(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	
	// will call delete itself...
	if (prj) new gtk_file_save_graphics_dialog(prj);
}

void gtk_project::popup_FileExtra1(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	
	// will call delete itself...
	if (prj) new gtk_importpdb_dialog(prj);
}

void gtk_project::popup_FileExtra2(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	
// this is just a stub for a new "extra"-item...
// this is just a stub for a new "extra"-item...
// this is just a stub for a new "extra"-item...
	
	if (prj)
	{
		prj->Message("this is not yet implemented...");
	}
}

void gtk_project::popup_SelectAll(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	prj->SelectAll();
}

void gtk_project::popup_SelectNone(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	prj->SelectAll();	// should call the base class function to prevent the flash!!!
	prj->InvertSelection();
}

void gtk_project::popup_InvertSelection(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	prj->InvertSelection();
}

void gtk_project::popup_HideSelected(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	prj->HideSelected();
}

void gtk_project::popup_ShowSelected(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	prj->ShowSelected();
}

void gtk_project::popup_LockSelected(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	prj->LockSelected();
}

void gtk_project::popup_UnlockSelected(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	prj->UnlockSelected();
}

void gtk_project::popup_DeleteSelected(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	prj->DeleteSelected();
}

void gtk_project::popup_SelectModeAtom(GtkWidget *, gpointer data)
{
	custom_app::current_select_mode = custom_app::smAtom;
cout << "DEBUG : selection mode = atm" << endl;
}

void gtk_project::popup_SelectModeResidue(GtkWidget *, gpointer data)
{
	custom_app::current_select_mode = custom_app::smResidue;
cout << "DEBUG : selection mode = res" << endl;
}

void gtk_project::popup_SelectModeChain(GtkWidget *, gpointer data)
{
	custom_app::current_select_mode = custom_app::smChain;
cout << "DEBUG : selection mode = chn" << endl;
}

void gtk_project::popup_SelectModeMolecule(GtkWidget *, gpointer data)
{
	custom_app::current_select_mode = custom_app::smMolecule;
cout << "DEBUG : selection mode = mol" << endl;
}

void gtk_project::popup_ViewsNewELD(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		prj->AddEnergyLevelDiagramClient(true);
	}
}

void gtk_project::popup_ViewsNewSSC(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		prj->AddGenericProteinChainClient(true);
	}
}

void gtk_project::popup_RModeBallStick(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->render = RENDER_BALL_AND_STICK;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_RModeVanDerWaals(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->render = RENDER_VAN_DER_WAALS;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_RModeCylinders(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->render = RENDER_CYLINDERS;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_RModeWireframe(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->render = RENDER_WIREFRAME;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_RModeNothing(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->render = RENDER_NOTHING;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_CModeElement(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->colormode = & project::cm_element;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_CModeSecStruct(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->colormode = & project::cm_secstruct;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_CModeHydPhob(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->colormode = & project::cm_hydphob;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_LModeIndex(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->label = LABEL_INDEX;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_LModeElement(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->label = LABEL_ELEMENT;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_LModeFCharge(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->label = LABEL_F_CHARGE;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_LModePCharge(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->label = LABEL_P_CHARGE;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_LModeAtomType(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->label = LABEL_ATOMTYPE;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_LModeBuilderID(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->label = LABEL_BUILDER_ID;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_LModeBondType(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->label = LABEL_BONDTYPE;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_LModeResidue(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->label = LABEL_RESIDUE;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_LModeSecStruct(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->label = LABEL_SEC_STRUCT;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_LModeNothing(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	oglwcl->label = LABEL_NOTHING;
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_project::popup_ObjRibbon(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->ref_civ) prj->UpdateChains();
		vector<chn_info> & ci_vector = (* prj->ref_civ);
		for (i32u n1 = 0;n1 < ci_vector.size();n1++)
		{
			if (ci_vector[n1].GetType() != chn_info::amino_acid) continue;
			if (ci_vector[n1].GetLength() < 3) continue;
			
			if (ci_vector[n1].GetSecStrStates() == NULL) DefineSecondaryStructure(prj);
			
			prj->AddObject(new ogl_ribbon(prj, oglwcl->colormode, n1, 4));		// min. order is 2!!!
		}
		
		prj->UpdateAllGraphicsViews();
	}
}

void gtk_project::popup_ObjEPlane(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add plane esp rb1 138.0 AUTO 1.0 50 1 0.75";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjEVolume(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add volrend esp rb2 138.0 0.0 1.0 25 0.50";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjESurface(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add surf2 esp unity red blue +35.0 -35.0 1.0 0.0 2.0 50 0 0 0.50";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjEVDWSurface(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add surf1 vdws esp rb1 1.0 70.0 AUTO 2.0 50 1 1 0.65";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjEDPlane(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add plane eldens rb1 0.05 0.0 0.75 50 1 0.75";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjEDSurface(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add surf1 eldens unity red 0.01 1.0 0.0 1.5 50 0 0 0.65";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjMOPlane(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add plane mo rb1 0.05 0.0 0.75 50 1 0.75";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjMOVolume(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add volrend mo rb2 0.025 0.0 1.5 25 0.50";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjMOSurface(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add surf2 mo unity red blue +0.025 -0.025 1.0 0.0 1.5 50 0 0 0.50";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjMODPlane(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add plane mod rb1 0.005 0.0 0.75 50 1 0.75";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjMODVolume(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add volrend mod rb2 0.0025 0.0 1.5 25 0.35";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjMODSurface(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (!prj->GetCurrentSetup()->GetCurrentEngine())
		{
			prj->Message(_("Please calculate energy first!"));
		}
		else
		{
			static const char command[] = "add surf1 mod unity red 0.0025 1.0 0.0 1.5 50 0 0 0.65";
			new gtk_command_dialog(prj, oglwcl, command);
		}
	}
}

void gtk_project::popup_ObjectsDeleteCurrent(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	prj->DoDeleteCurrentObject();
}

void gtk_project::popup_CompSetup(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	
	// will call delete itself...
	new gtk_setup_dialog(prj);
}

void gtk_project::popup_CompEnergy(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj) prj->DoEnergy();
}

void gtk_project::popup_CompGeomOpt(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		setup * su = prj->GetCurrentSetup();
		static jobinfo_GeomOpt ji;
		
		ji.prj = prj;
		ji.go = geomopt_param(su);
		ji.show_dialog = true;
		
		prj->start_job_GeomOpt(& ji);
	}
}

void gtk_project::popup_CompMolDyn(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		setup * su = prj->GetCurrentSetup();
		static jobinfo_MolDyn ji;
		
		ji.prj = prj;
		ji.md = moldyn_param(su);
		ji.show_dialog = true;
		
		prj->start_job_MolDyn(& ji);
	}
}

void gtk_project::popup_CompRandomSearch(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		static const char command[] = "random_search 100 250";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_CompSystematicSearch(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		static const char command[] = "systematic_search 6 250";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_CompMonteCarloSearch(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		static const char command[] = "montecarlo_search 10 100 250";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_CompTorsionEnergyPlot1D(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
prj->Message(_("PLEASE NOTE!\nThe command string, which is displayed in the next dialog, is incomplete.\nYou should replace the letters A-D with atom indices that define the torsion.\n\nALSO NOTE: structure refinement is always done using molecular mechanics (optsteps)."));
		
		static const char command[] = "make_plot1 A B C D 36 0.0 360.0 250";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_CompTorsionEnergyPlot2D(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
prj->Message(_("PLEASE NOTE!\nThe command string, which is displayed in the next dialog, is incomplete.\nYou should replace the letters A-D and I-L with atom indices that define the torsions.\n\nALSO NOTE: structure refinement is always done using molecular mechanics (optsteps)."));
		
		static const char command[] = "make_plot2 A B C D 36 0.0 360.0 I J K L 36 0.0 360.0 250";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_CompPopAnaElectrostatic(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		static const char command[] = "population_analysis_ESP";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_CompTransitionStateSearch(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		static const char command[] = "transition_state_search 10.0 500.0";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_CompStationaryStateSearch(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		static const char command[] = "stationary_state_search 100";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_CompFormula(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj) prj->DoFormula();
}

void gtk_project::popup_CompSetFormalCharge(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		static const char command[] = "set_formal_charge X +0";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_CompCreateRS(GtkWidget *, gpointer data)	// todo : this is only for testing?!?!?!?
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		if (prj->GetRS() == NULL) prj->CreateRS();
	}
}

void gtk_project::popup_CompCycleRS(GtkWidget *, gpointer data)	// todo : this is only for testing?!?!?!?
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj && prj->GetRS() != NULL)
	{
	//	prj->GetRS()->CycleStructures();
		prj->UpdateAllGraphicsViews();
	}
	else cout << _("ERROR") << endl;
}

void gtk_project::popup_TrajView(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (!prj->GetTrajectoryFile())
	{
		// will call delete itself...
		if (prj) new gtk_trajfile_dialog(prj);
	}
	else prj->ErrorMessage(_("Trajectory file already open?"));
}

void gtk_project::popup_SetOrbital(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		prj->Message(_("PLEASE NOTE!\nThe command string, which is displayed in the next dialog, is incomplete.\nYou should replace the letter X with the orbital index that will become the current orbital."));
		
		static const char command[] = "set_current_orbital X";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_HAdd(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		prj->AddH();
		prj->UpdateAllGraphicsViews();
	}
}

void gtk_project::popup_HRemove(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		prj->RemoveH();
		prj->UpdateAllGraphicsViews();
	}
}

void gtk_project::popup_SolvateBox(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		static const char command[] = "solvate_box 3.0 3.0 3.0";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_SolvateSphere(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		static const char command[] = "solvate_sphere 1.2 1.6";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_BuilderAmino(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
prj->Message(_("PLEASE NOTE!\nThe command string, which is displayed in the next dialog, is incomplete.\nYou should replace the default sequence AAA with the sequence to be built."));
		
		static const char command[] = "build_amino AAA";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_BuilderNucleic(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
prj->Message(_("PLEASE NOTE!\nThe command string, which is displayed in the next dialog, is incomplete.\nYou should replace the default sequence AGTCaguc with the sequence to be built."));
		
		static const char command[] = "build_nucleic AGTCaguc";
		new gtk_command_dialog(prj, oglwcl, command);
	}
}

void gtk_project::popup_Center(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		prj->CenterCRDSet(0, true);
		
		// Which is the current Coord Set?
		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		// usually it's 0, but it could be any/all.
		// the crdset-system is not yet in GUI...
		
		// 2001-06-18 TH: yes, this is not quite ready.
		// but i guess in the end we will move ALL crd-sets...
		
// sometimes this centering won't work, if the camera won't point to the origo.
// so, here we try to turn it there. ANY EFFECTS TO LIGHTS, ETC??????
		
		// how to get base_app::camera_vector in a reasonable way?
		// it's easy to set the focus of camera but what about lights???
		
		// -> implement this stuff in base_app????????????????????????
		
		prj->UpdateAllGraphicsViews();
	}
}

void gtk_project::popup_ClearAll(GtkWidget *, gpointer data)
{
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj && base_app::GetAppB()->Question(_("Are you sure you want to clear everything?")))
	{
		prj->ClearModel();
		prj->UpdateAllGraphicsViews();
	}
}

void gtk_project::popup_EnterCommand(GtkWidget *, gpointer data)
{
	oglview_wcl * oglwcl = GetClient((GtkWidget *) data);
	gtk_project * prj = gtk_app::GetPrjX();
	if (prj)
	{
		new gtk_command_dialog(prj, oglwcl, NULL);
	}
}

/*################################################################################################*/

// eof

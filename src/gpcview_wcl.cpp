// GPCVIEW_WCL.CPP

// Copyright (C) 2006 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gpcview_wcl.h"

/*################################################################################################*/

gpcview_wcl::gpcview_wcl(void) :
	pangofont_wcl(new ogl_camera(ogl_ol_static(), 1.0))
{
}

gpcview_wcl::~gpcview_wcl(void)
{
	// problem : lifetime of the camera object needs to be longer than
	// lifetime of this object since it is needed at the base class dtor.
	// solution : ask the base class to do the cleanup work for us...
	
	delete_cam_plz = true;
}

void gpcview_wcl::ButtonEvent(int, int)
{
}

void gpcview_wcl::MotionEvent(int, int)
{
}

void gpcview_wcl::UpdateWnd(void)
{
	base_wnd * wnd = GetWnd();
	if (!wnd || wnd->GetWidth() < 0 || !cam) return;
	
	wnd->SetCurrent();
	cam->RenderScene(wnd, false, false);
}

void gpcview_wcl::InitGL(void)
{
	// all classes that inherit pangofont_wcl must call ogl_InitPangoFont()!!!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	ogl_InitPangoFont("courier 12");
}

void gpcview_wcl::RenderGL(rmode)
{
}

/*################################################################################################*/

// eof

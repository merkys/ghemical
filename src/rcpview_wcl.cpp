// RCPVIEW_WCL.CPP

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "rcpview_wcl.h"

#include "custom_app.h"

#include "local_i18n.h"

#include <sstream>
using namespace std;

/*################################################################################################*/

rcpview_wcl::rcpview_wcl(const char * s1, const char * sv) : 
	p1dview_wcl(s1, sv)
{
	ready = false;
	
	r_treshold = 0.65;	// default for range 0<->1...
	p_treshold = 0.35;	// default for range 0<->1...
}

rcpview_wcl::~rcpview_wcl(void)
{
}

void rcpview_wcl::Finalize(void)
{
	sort(dv.begin(), dv.end());
	
	i32s index = 0; fGL maxv = dv.front().v;
	for (i32s n1 = 1;n1 < (i32s) dv.size();n1++)
	{
		if (dv[n1].v > maxv)
		{
			index = n1;
			maxv = dv[n1].v;
		}
	}
	
	// first calculate the final reaction coordinates...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	dv[index].c1 = 0.0;	// should already be zero...
	
	int negcount = -3;	//???
	for (i32s n1 = index - 1;n1 >= 0;n1--)
	{
		dv[n1].c1 = negcount--;
	}
	
	int poscount = +3;	//???
	for (i32s n1 = index + 1;n1 < (i32s) dv.size();n1++)
	{
		dv[n1].c1 = poscount++;
	}
	
/*	here we try to calculate distances from TS ; it gives a good
	scale but sometimes the order of points will change.
	TODO : measure 4 distances for scaling purposes...
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	fGL * c_ts = (fGL *) (((char *) dv[index].udata) + sizeof(i32s));
	
	fGL minrc = 0.0; fGL maxrc = 0.0;
	for (i32u n1 = 0;n1 < dv.size();n1++)
	{
		if (n1 == (i32u) index)
		{
			dv[n1].c1 = 0.0;
			continue;
		}
		
		fGL * c_xx = (fGL *) (((char *) dv[n1].udata) + sizeof(i32s));
		
		fGL rc = 0.0;
		for (i32u n5 = 0;n5 < patoms.size();n5++)
		{
			const fGL * crd1 = & c_ts[patoms[n5] * 3];
			const fGL * crd2 = & c_xx[patoms[n5] * 3];
			
			for (i32u n6 = 0;n6 < 3;n6++)
			{
				fGL tmp1 = crd2[n6] - crd1[n6];
				rc += tmp1 * tmp1;
			}
		}
		
		rc = sqrt(rc);
		if (n1 < (i32u) index) rc = -rc;
		
		if (rc < minrc) minrc = rc;
		if (rc > maxrc) maxrc = rc;
		
		dv[n1].c1 = rc;
	}	*/
	
// what the heck?!?!?! plot1d_view is unable to display negative values???
// then, just re-scale them form 0 to 1. THIS IS A BIT STRANGE...
	
	fGL minrc = 0.0; fGL maxrc = 0.0;
	for (i32u n1 = 0;n1 < dv.size();n1++)
	{
		fGL rc = dv[n1].c1;
		if (rc < minrc) minrc = rc;
		if (rc > maxrc) maxrc = rc;
	}
	
	for (i32u n1 = 0;n1 < dv.size();n1++)
	{
		fGL rc = (dv[n1].c1 - minrc) / (maxrc - minrc);
		dv[n1].c1 = rc;
	}
	
	// ...and then update the display tresholds.
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	const f64 eri = dv.front().v;
	const f64 epi = dv.back().v;
	
	f64 limit_r = (epi + maxv) / 2.0;
	for (i32s n1 = index;n1 < (i32s) dv.size();n1++)
	{
		if (dv[n1].v < limit_r)
		{
			r_treshold = (dv[n1].c1 + dv[n1 - 1].c1) / 2.0;
			break;
		}
	}
	
	f64 limit_p = (eri + maxv) / 2.0;
	for (i32s n1 = index;n1 >= 0;n1--)
	{
		if (dv[n1].v < limit_p)
		{
			p_treshold = (dv[n1].c1 + dv[n1 + 1].c1) / 2.0;
			break;
		}
	}
	
	project * prj = custom_app::GetAppC()->GetPrj();
	
	ostringstream txts;
	txts << _("the tresholds were set to ") << r_treshold << _(" and ") << p_treshold << endl << ends;
	prj->PrintToLog(txts.str().c_str());
	
	ready = true;
	
	// finally do the base class initializaion tasks...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	p1dview_wcl::Finalize();
}

void rcpview_wcl::ButtonEvent(int x, int y)
{
	if (!ready) return;
	
	base_wnd * wnd = GetWnd();
	
	// first setup the bonds...
	// ^^^^^^^^^^^^^^^^^^^^^^^^
	
	if (!(wnd->GetWidth() > 1)) return;
	fGL sx = 1.10 * (fGL) x / (fGL) wnd->GetWidth() - 0.05;
	if (sx < 0.0) return; if (sx > 1.0) return;
	
	fGL sel1 = sx * (max1 - min1);
	
	bool show_r_bonds = (sel1 < r_treshold);
	for (i32u n1 = 0;n1 < rbonds.size();n1++)
	{
		rbonds[n1]->do_not_render_TSS_fixmelater = !show_r_bonds;
	}
	
	bool show_p_bonds = (sel1 > p_treshold);
	for (i32u n1 = 0;n1 < pbonds.size();n1++)
	{
		pbonds[n1]->do_not_render_TSS_fixmelater = !show_p_bonds;
	}
	
	// ...and finally call the base class ButtonEvent().
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	p1dview_wcl::ButtonEvent(x, y);
}

void rcpview_wcl::MotionEvent(int x, int y)
{
	p1dview_wcl::MotionEvent(x, y);
}

/*################################################################################################*/

// eof

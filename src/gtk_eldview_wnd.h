// GTK_ELDVIEW_WND.H : write a short description here...

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ghemicalconfig2.h"

#ifndef GTK_ELDVIEW_WND_H
#define GTK_ELDVIEW_WND_H

#include "gtk_wnd.h"

/*################################################################################################*/

class gtk_eldview_wnd :
	public gtk_wnd
{
	private:
	
	static GtkActionEntry entries[];
	static const char * ui_description;
	
	public:
	
	gtk_eldview_wnd(bool);
	~gtk_eldview_wnd(void);
	
	static void popup_AttachDetach(GtkWidget *, gpointer);
	static void popup_DeleteView(GtkWidget *, gpointer);
};

/*################################################################################################*/

#endif	// GTK_ELDVIEW_WND_H

// eof

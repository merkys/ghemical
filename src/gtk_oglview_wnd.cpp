// GTK_OGLVIEW_WND.CPP

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_oglview_wnd.h"

#include "oglview_wcl.h"
#include "gtk_project.h"

#include "gtk_simple_dialogs.h"

#include "local_i18n.h"

#include <ghemical/utility.h>

/*################################################################################################*/

GtkActionEntry gtk_oglview_wnd::entries[] =
{
	{ "AttachDetachGV", NULL, N_("Attach/Detach This View"), NULL, N_("Select whether this view is attached or in an independent window"), (GCallback) gtk_oglview_wnd::popup_ViewsAttachDetach },
	
	{ "FileMenu", NULL, N_("File") },
	// ^^^^^^^^
	{ "Open", GTK_STOCK_OPEN, N_("Open"), NULL, N_("Open a file"), (GCallback) gtk_app::mainmenu_FileOpen },
#ifdef ENABLE_OPENBABEL
	{ "Import", GTK_STOCK_OPEN, N_("Import..."), NULL, N_("Import other file formats using libopenbabel"), (GCallback) gtk_project::popup_FileImport },
#endif	// ENABLE_OPENBABEL
	{ "SaveAs", GTK_STOCK_SAVE, N_("Save as..."), NULL, N_("Save a file"), (GCallback) gtk_app::mainmenu_FileSaveAs },
#ifdef ENABLE_OPENBABEL
	{ "Export", GTK_STOCK_SAVE, N_("Export"), NULL, N_("Export other file formats using OpenBabel"), (GCallback) gtk_project::popup_FileExport },
#endif	// ENABLE_OPENBABEL
//	{ "ExportGraphics", GTK_STOCK_SAVE, N_("Export Graphics"), NULL, N_("Export graphics"), (GCallback) gtk_project::popup_FileExportGraphics },
	{ "Close", GTK_STOCK_QUIT, N_("Close"), NULL, N_("Close the file"), (GCallback) gtk_app::mainmenu_FileClose },
	
	{ "FileExtraMenu", NULL, N_("Extra Tools") },
	// ^^^^^^^^^^^^^
	{ "FileExtra1", NULL, N_("Import PDB"), NULL, N_("Import a PDB file"), (GCallback) gtk_project::popup_FileExtra1 },
//	{ "FileExtra2", NULL, N_("Import ???"), NULL, N_("Import a ??? file"), (GCallback) gtk_project::popup_FileExtra2 },
	
	{ "SelectMenu", NULL, N_("Select") },
	// ^^^^^^^^^^
	{ "SelectAll", NULL, N_("Select All"), NULL, N_("Select all atoms"), (GCallback) gtk_project::popup_SelectAll },
	{ "SelectNone", NULL, N_("Select None"), NULL, N_("Select nothing"), (GCallback) gtk_project::popup_SelectNone },
	{ "InvertSelection", NULL, N_("Invert Selection"), NULL, N_("Invert the selection"), (GCallback) gtk_project::popup_InvertSelection },
	{ "HideSelected", NULL, N_("Hide Selected"), NULL, N_("Hide all selected atoms"), (GCallback) gtk_project::popup_HideSelected },
	{ "ShowSelected", NULL, N_("Show Selected"), NULL, N_("Show all selected atoms"), (GCallback) gtk_project::popup_ShowSelected },
	{ "LockSelected", NULL, N_("Lock Selected"), NULL, N_("Lock all selected atoms"), (GCallback) gtk_project::popup_LockSelected },
	{ "UnlockSelected", NULL, N_("Unlock Selected"), NULL, N_("Unlock all selected atoms"), (GCallback) gtk_project::popup_UnlockSelected },
	{ "DeleteSelected", NULL, N_("Delete Selected"), NULL, N_("Delete all selected atoms"), (GCallback) gtk_project::popup_DeleteSelected },
	
	{ "SelectModeMenu", NULL, N_("Selection Mode") },
	// ^^^^^^^^^^^^^^
// todo : convert these into toggle items so that the setting from project is show in each of the graphics views...
	{ "SelectAtoms", NULL, N_("Select Atoms"), NULL, N_("Select single atoms"), (GCallback) gtk_project::popup_SelectModeAtom },
	{ "SelectResidues", NULL, N_("Select Residues"), NULL, N_("Select residues"), (GCallback) gtk_project::popup_SelectModeResidue },
	{ "SelectChains", NULL, N_("Select Chains"), NULL, N_("Select chains"), (GCallback) gtk_project::popup_SelectModeChain },
	{ "SelectMolecules", NULL, N_("Select Molecules"), NULL, N_("Select molecules"), (GCallback) gtk_project::popup_SelectModeMolecule },
	
	{ "RenderMenu", NULL, N_("Render") },
	// ^^^^^^^^^^
// todo : convert these into toggle items so that the local setting is show in each of the graphics views...
	{ "RenderQuickUpdate", NULL, N_("Quick Update Mode"), NULL, N_("Quick Update Mode switches into Wireframe rendering mode when doing interactive work"), (GCallback) gtk_oglview_wnd::popup_RenderQuickUpdate },
	
	{ "RendViewsMenu", NULL, N_("Views") },
	// ^^^^^^^^^^^^^
	{ "RendViewsDeleteView", NULL, N_("Delete This View"), NULL, N_("Delete this graphics view"), (GCallback) gtk_oglview_wnd::popup_ViewsDeleteView },
	{ "RendViewsPushCRDSet", NULL, N_("Save a CRD-set (experimental)"), NULL, N_("Save a coordinate set"), (GCallback) gtk_oglview_wnd::popup_ViewsPushCRDSet },
	{ "RendViewsSuperimpose", NULL, N_("Superimpose CRD-sets (experimental)"), NULL, N_("Superimpose coordinate sets"), (GCallback) gtk_oglview_wnd::popup_ViewsSuperimpose },
	
	{ "RendViewsCreateMenu", NULL, N_("Create") },
	// ^^^^^^^^^^^^^^^^^^^
	{ "RendViewsCreateWnd", NULL, N_("New Window"), NULL, N_("Create a new view using the current camera"), (GCallback) gtk_oglview_wnd::popup_ViewsNewWnd },
	{ "RendViewsCreateCam", NULL, N_("New Camera"), NULL, N_("Create a new view using a new camera"), (GCallback) gtk_oglview_wnd::popup_ViewsNewCam },
	{ "RendViewsCreateELD", NULL, N_("Energy-level Diagram"), NULL, N_("Create a new energy-level diagram view"), (GCallback) gtk_project::popup_ViewsNewELD },
	{ "RendViewsCreateSSC", NULL, N_("Secondary Structure Constraints"), NULL, N_("Create a new secondary structure constraints view"), (GCallback) gtk_project::popup_ViewsNewSSC },
	
	{ "RendLightsMenu", NULL, N_("Lights") },
	// ^^^^^^^^^^^^^^
	{ "RendLightsNewLight", NULL, N_("Create New Light"), NULL, N_("Create a new light object"), (GCallback) gtk_oglview_wnd::popup_LightsNewLight },
// todo : convert these into toggle items so that the local setting is show in each of the graphics views...
	{ "RendLightsEnableLocLights", NULL, N_("Enable Local Lights"), NULL, N_("Switch on/off the local light objects"), (GCallback) gtk_oglview_wnd::popup_LightsSwitchLoc },
	{ "RendLightsEnableGlobLights", NULL, N_("Enable Global Lights"), NULL, N_("Switch on/off the global light objects"), (GCallback) gtk_oglview_wnd::popup_LightsSwitchGlob },
	
	{ "RendRenderMenu", NULL, N_("Render Mode") },
	// ^^^^^^^^^^^^^^
// todo : convert these into toggle items so that the local setting is show in each of the graphics views...
	{ "RendRenderBallStick", NULL, N_("Ball & Stick"), NULL, N_("Display atoms and bonds using Ball & Stick mode"), (GCallback) gtk_project::popup_RModeBallStick },
	{ "RendRenderVanDerWaals", NULL, N_("van der Waals"), NULL, N_("Display atoms and bonds using van der Waals mode"), (GCallback) gtk_project::popup_RModeVanDerWaals },
	{ "RendRenderCylinders", NULL, N_("Cylinders"), NULL, N_("Display atoms and bonds using Cylinders mode"), (GCallback) gtk_project::popup_RModeCylinders },
	{ "RendRenderWireframe", NULL, N_("Wireframe"), NULL, N_("Display atoms and bonds using Wireframe mode"), (GCallback) gtk_project::popup_RModeWireframe },
	{ "RendRenderNothing", NULL, N_("Nothing"), NULL, N_("Do not display atoms and bonds"), (GCallback) gtk_project::popup_RModeNothing },
	
	{ "RendColorMenu", NULL, N_("Color Mode") },
	// ^^^^^^^^^^^^^
// todo : convert these into toggle items so that the local setting is show in each of the graphics views...
	{ "RendColorElement", NULL, N_("Element"), NULL, N_("Color atoms and bonds by element"), (GCallback) gtk_project::popup_CModeElement },
	{ "RendColorSecStruct", NULL, N_("Sec-Struct"), NULL, N_("Color atoms and bonds by secondary-structure state"), (GCallback) gtk_project::popup_CModeSecStruct },
	{ "RendColorHydPhob", NULL, N_("Hydrophobicity"), NULL, N_("Color atoms and bonds by hydrophobicity (experimental)"), (GCallback) gtk_project::popup_CModeHydPhob },
	
	{ "RendLabelMenu", NULL, N_("Label Mode") },
	// ^^^^^^^^^^^^^
// todo : convert these into toggle items so that the local setting is show in each of the graphics views...
	{ "RendLabelIndex", NULL, N_("Index"), NULL, N_("Label atoms by atom index"), (GCallback) gtk_project::popup_LModeIndex },
	{ "RendLabelElement", NULL, N_("Element"), NULL, N_("Label atoms by element"), (GCallback) gtk_project::popup_LModeElement },
	{ "RendLabelFCharge", NULL, N_("Formal Charge"), NULL, N_("Label atoms by formal charge"), (GCallback) gtk_project::popup_LModeFCharge },
	{ "RendLabelPCharge", NULL, N_("Partial Charge"), NULL, N_("Label atoms by partial charge"), (GCallback) gtk_project::popup_LModePCharge },
	{ "RendLabelAtomType", NULL, N_("Atom Type"), NULL, N_("Label atoms by atom type"), (GCallback) gtk_project::popup_LModeAtomType },
	{ "RendLabelBuilderID", NULL, N_("Builder ID"), NULL, N_("Label atoms by sequence builder ID"), (GCallback) gtk_project::popup_LModeBuilderID },
	{ "RendLabelBondType", NULL, N_("Bond Type"), NULL, N_("Label bonds by bond type"), (GCallback) gtk_project::popup_LModeBondType },
	{ "RendLabelResidue", NULL, N_("Residue"), NULL, N_("Label by residue"), (GCallback) gtk_project::popup_LModeResidue },
	{ "RendLabelSecStruct", NULL, N_("Sec-Struct"), NULL, N_("Label by secondary structure state"), (GCallback) gtk_project::popup_LModeSecStruct },
	{ "RendLabelNothing", NULL, N_("Nothing"), NULL, N_("Do not show labels"), (GCallback) gtk_project::popup_LModeNothing },
	
	{ "RendProjectionMenu", NULL, N_("Projection") },
	// ^^^^^^^^^^^^^^^^^^
// todo : convert these into toggle items so that the local setting is show in each of the graphics views...
	{ "RendProjOrthographic", NULL, N_("Orthographic"), NULL, N_("Use orthographic projection"), (GCallback) gtk_oglview_wnd::popup_ProjOrthographic },
	{ "RendProjPerspective", NULL, N_("Perspective"), NULL, N_("Use perspective projection"), (GCallback) gtk_oglview_wnd::popup_ProjPerspective },
// todo : convert these into toggle items so that the local setting is show in each of the graphics views...
// todo : add more color combinations ; red/green red/blue ; any others needed???
	{ "RendProjSterRedBlue", NULL, N_("Red-Blue Stereo"), NULL, N_("Use red-blue stereo mode"), (GCallback) gtk_oglview_wnd::popup_ProjSterRedBlue },
	{ "RendProjSterRelaxed", NULL, N_("Relaxed-Eye Stereo"), NULL, N_("Use relaxed-eye stereo mode"), (GCallback) gtk_oglview_wnd::popup_ProjSterRelaxed },
	
	{ "ObjectsMenu", NULL, N_("Objects") },
	// ^^^^^^^^^^^
	{ "ObjectsRibbon", NULL, N_("Ribbon"), NULL, N_("Create a Ribbon object for displaying peptides/proteins"), (GCallback) gtk_project::popup_ObjRibbon },
	{ "ObjectsEPlane", NULL, N_("ESP-plane"), NULL, N_("Create an ESP-plane object"), (GCallback) gtk_project::popup_ObjEPlane },
	{ "ObjectsEVolume", NULL, N_("volume-rendered ESP"), NULL, N_("Create a volume-rendered ESP object"), (GCallback) gtk_project::popup_ObjEVolume },
	{ "ObjectsESurface", NULL, N_("ESP-surface"), NULL, N_("Create an ESP-surface object"), (GCallback) gtk_project::popup_ObjESurface },
	{ "ObjectsEVDWSurface", NULL, N_("ESP-colored vdW-surface"), NULL, N_("Create an ESP-colored vdW-surface object"), (GCallback) gtk_project::popup_ObjEVDWSurface },
	{ "ObjectsEDPlane", NULL, N_("Electron density plane"), NULL, N_("Create an Electron density plane object"), (GCallback) gtk_project::popup_ObjEDPlane },
	{ "ObjectsEDSurface", NULL, N_("Electron density surface"), NULL, N_("Create an Electron density surface object"), (GCallback) gtk_project::popup_ObjEDSurface },
	{ "ObjectsMOPlane", NULL, N_("Molecular orbital plane"), NULL, N_("Create a Molecular orbital plane object"), (GCallback) gtk_project::popup_ObjMOPlane },
	{ "ObjectsMOVolume", NULL, N_("Molecular orbital volume"), NULL, N_("Create a Molecular orbital volume object"), (GCallback) gtk_project::popup_ObjMOVolume },
	{ "ObjectsMOSurface", NULL, N_("Molecular orbital surface"), NULL, N_("Create a Molecular orbital surface object"), (GCallback) gtk_project::popup_ObjMOSurface },
	{ "ObjectsMODPlane", NULL, N_("MO-density plane"), NULL, N_("Create an MO-density plane object"), (GCallback) gtk_project::popup_ObjMODPlane },
	{ "ObjectsMODVolume", NULL, N_("MO-density volume"), NULL, N_("Create an MO-density volume object"), (GCallback) gtk_project::popup_ObjMODVolume },
	{ "ObjectsMODSurface", NULL, N_("MO-density surface"), NULL, N_("Create an MO-density surface object"), (GCallback) gtk_project::popup_ObjMODSurface },
	{ "ObjectsDeleteCurrent", NULL, N_("Delete Current Object"), NULL, N_("Delete the currently selected object"), (GCallback) gtk_project::popup_ObjectsDeleteCurrent },
	
	{ "ComputeMenu", NULL, N_("Compute") },
	// ^^^^^^^^^^^
	{ "CompSetup", NULL, N_("Setup..."), NULL, "Setup or change the comp.chem. method in use", (GCallback) gtk_project::popup_CompSetup },
	{ "CompEnergy", NULL, N_("Energy"), NULL, "Compute a single-point energy", (GCallback) gtk_project::popup_CompEnergy },
	{ "CompGeomOpt", NULL, N_("Geometry Optimization..."), NULL, "Do a geometry optimization run", (GCallback) gtk_project::popup_CompGeomOpt },
	{ "CompMolDyn", NULL, N_("Molecular Dynamics..."), NULL, "Do a molecular dynamics run", (GCallback) gtk_project::popup_CompMolDyn },
	{ "CompRandomSearch", NULL, N_("Random Conformational Search..."), NULL, "Do a random conformational search", (GCallback) gtk_project::popup_CompRandomSearch },
	{ "CompSystematicSearch", NULL, N_("Systematic Conformational Search..."), NULL, "Do a random conformational search", (GCallback) gtk_project::popup_CompSystematicSearch },
	{ "CompMonteCarloSearch", NULL, N_("Monte Carlo Search..."), NULL, "Do a Monte Carlo type conformational search", (GCallback) gtk_project::popup_CompMonteCarloSearch },
	{ "CompTorEnePlot1D", NULL, N_("Plot Energy vs. 1 Torsion Angle..."), NULL, "...todo...", (GCallback) gtk_project::popup_CompTorsionEnergyPlot1D },
	{ "CompTorEnePlot2D", NULL, N_("Plot Energy vs. 2 Torsion Angles..."), NULL, "...todo...", (GCallback) gtk_project::popup_CompTorsionEnergyPlot2D },
	{ "CompPopAnaESP", NULL, N_("Population Analysis (ESP)"), NULL, "...todo...", (GCallback) gtk_project::popup_CompPopAnaElectrostatic },
	{ "CompTransitionSS", NULL, N_("Transition State Search..."), NULL, "...todo...", (GCallback) gtk_project::popup_CompTransitionStateSearch },
	{ "CompStationarySS", NULL, N_("Stationary State Search..."), NULL, "...todo...", (GCallback) gtk_project::popup_CompStationaryStateSearch },
	{ "CompFormula", NULL, N_("Formula"), NULL, "...todo...", (GCallback) gtk_project::popup_CompFormula },
{ "CompUC_SetFormalCharge", NULL, "UnderConstruction ; RS ; Set Formal Charge", NULL, "...todo...", (GCallback) gtk_project::popup_CompSetFormalCharge },
{ "CompUC_CreateRS", NULL, "UnderConstruction ; RS ; make RS", NULL, "...todo...", (GCallback) gtk_project::popup_CompCreateRS },
{ "CompUC_CycleRS", NULL, "UnderConstruction ; RS ; cycle RS", NULL, "...todo...", (GCallback) gtk_project::popup_CompCycleRS },
	
	{ "TrajView", NULL, N_("MD Trajectory Viewer..."), NULL, "...todo...", (GCallback) gtk_project::popup_TrajView },
	{ "SetOrbital", NULL, N_("Set Current Orbital"), NULL, "...todo...", (GCallback) gtk_project::popup_SetOrbital },
	
	{ "BuildMenu", NULL, N_("Build") },
	// ^^^^^^^^^
	{ "BuildSolvateBox", NULL, N_("Solvate Box..."), NULL, "...todo...", (GCallback) gtk_project::popup_SolvateBox },
	{ "BuildSolvateSphere", NULL, N_("Solvate Sphere..."), NULL, "...todo...", (GCallback) gtk_project::popup_SolvateSphere },
	{ "BuildSeqBuildAmino", NULL, N_("Sequence Builder (amino)..."), NULL, "...todo...", (GCallback) gtk_project::popup_BuilderAmino },
	{ "BuildSeqBuildNucleic", NULL, N_("Sequence Builder (nucleic)..."), NULL, "...todo...", (GCallback) gtk_project::popup_BuilderNucleic },
	{ "BuildCenter", NULL, N_("Center"), NULL, "...todo...", (GCallback) gtk_project::popup_Center },
	{ "BuildClear", NULL, N_("Zap All"), NULL, "...todo...", (GCallback) gtk_project::popup_ClearAll },
	
	{ "BuildHydrogensMenu", NULL, N_("Hydrogens") },
	// ^^^^^^^^^^^^^^^^^^
	{ "BuildHydrogensAdd", NULL, N_("Add"), NULL, "...todo...", (GCallback) gtk_project::popup_HAdd },
	{ "BuildHydrogensRemove", NULL, N_("Remove"), NULL, "...todo...", (GCallback) gtk_project::popup_HRemove },
	
	{ "EnterCommand", NULL, N_("Enter a Command..."), NULL, "...todo...", (GCallback) gtk_project::popup_EnterCommand },
};

const char * gtk_oglview_wnd::ui_description =
"<ui>"
"  <popup name='ggvMenu'>"
"    <menuitem action='AttachDetachGV'/>"
"    <separator/>"
"    <menu action='FileMenu'>"
"      <menuitem action='Open'/>"
#ifdef ENABLE_OPENBABEL
"      <menuitem action='Import'/>"
#endif	// ENABLE_OPENBABEL
"      <separator/>"
"      <menuitem action='SaveAs'/>"
#ifdef ENABLE_OPENBABEL
"      <menuitem action='Export'/>"
#endif	// ENABLE_OPENBABEL
//"      <menuitem action='ExportGraphics'/>"
"      <separator/>"
"      <menu action='FileExtraMenu'>"
"        <menuitem action='FileExtra1'/>"
//"        <menuitem action='FileExtra2'/>"
"      </menu>"
"      <separator/>"
"      <menuitem action='Close'/>"
"    </menu>"
"    <menu action='SelectMenu'>"
"      <menuitem action='SelectAll'/>"
"      <menuitem action='SelectNone'/>"
"      <separator/>"
"      <menuitem action='InvertSelection'/>"
"      <separator/>"
"      <menu action='SelectModeMenu'>"
"        <menuitem action='SelectAtoms'/>"
"        <menuitem action='SelectResidues'/>"
"        <menuitem action='SelectChains'/>"
"        <menuitem action='SelectMolecules'/>"
"      </menu>"
"      <separator/>"
"      <menuitem action='HideSelected'/>"
"      <menuitem action='ShowSelected'/>"
"      <separator/>"
"      <menuitem action='LockSelected'/>"
"      <menuitem action='UnlockSelected'/>"
"      <separator/>"
"      <menuitem action='DeleteSelected'/>"
"    </menu>"
"    <separator/>"
"    <menu action='RenderMenu'>"
"      <menu action='RendViewsMenu'>"
"        <menu action='RendViewsCreateMenu'>"
"          <menuitem action='RendViewsCreateWnd'/>"
"          <menuitem action='RendViewsCreateCam'/>"
"          <separator/>"
"          <menuitem action='RendViewsCreateELD'/>"
"          <menuitem action='RendViewsCreateSSC'/>"
"        </menu>"
"        <menuitem action='RendViewsDeleteView'/>"
"        <separator/>"
"        <menuitem action='RendViewsPushCRDSet'/>"
"        <menuitem action='RendViewsSuperimpose'/>"
"      </menu>"
"      <menu action='RendLightsMenu'>"
"        <menuitem action='RendLightsNewLight'/>"
"        <separator/>"
"        <menuitem action='RendLightsEnableLocLights'/>"
"        <menuitem action='RendLightsEnableGlobLights'/>"
"      </menu>"
"      <separator/>"
"      <menu action='RendRenderMenu'>"
"        <menuitem action='RendRenderBallStick'/>"
"        <menuitem action='RendRenderVanDerWaals'/>"
"        <menuitem action='RendRenderCylinders'/>"
"        <menuitem action='RendRenderWireframe'/>"
"        <menuitem action='RendRenderNothing'/>"
"      </menu>"
"      <menu action='RendColorMenu'>"
"        <menuitem action='RendColorElement'/>"
"        <menuitem action='RendColorSecStruct'/>"
"        <menuitem action='RendColorHydPhob'/>"
"      </menu>"
"      <menu action='RendLabelMenu'>"
"        <menuitem action='RendLabelIndex'/>"
"        <menuitem action='RendLabelElement'/>"
"        <menuitem action='RendLabelFCharge'/>"
"        <menuitem action='RendLabelPCharge'/>"
"        <menuitem action='RendLabelAtomType'/>"
"        <menuitem action='RendLabelBuilderID'/>"
"        <separator/>"
"        <menuitem action='RendLabelBondType'/>"
"        <separator/>"
"        <menuitem action='RendLabelResidue'/>"
"        <menuitem action='RendLabelSecStruct'/>"
"        <separator/>"
"        <menuitem action='RendLabelNothing'/>"
"      </menu>"
"      <separator/>"
"      <menu action='RendProjectionMenu'>"
"        <menuitem action='RendProjOrthographic'/>"
"        <menuitem action='RendProjPerspective'/>"
"        <separator/>"
"        <menuitem action='RendProjSterRedBlue'/>"
"        <menuitem action='RendProjSterRelaxed'/>"
"      </menu>"
"      <menuitem action='RenderQuickUpdate'/>"
"    </menu>"
"    <menu action='ObjectsMenu'>"
"      <menuitem action='ObjectsRibbon'/>"
"      <separator/>"
"      <menuitem action='ObjectsEPlane'/>"
"      <menuitem action='ObjectsEVolume'/>"
"      <menuitem action='ObjectsESurface'/>"
"      <menuitem action='ObjectsEVDWSurface'/>"
"      <separator/>"
"      <menuitem action='ObjectsEDPlane'/>"
"      <menuitem action='ObjectsEDSurface'/>"
"      <menuitem action='ObjectsMOPlane'/>"
"      <menuitem action='ObjectsMOVolume'/>"
"      <menuitem action='ObjectsMOSurface'/>"
"      <menuitem action='ObjectsMODPlane'/>"
"      <menuitem action='ObjectsMODVolume'/>"
"      <menuitem action='ObjectsMODSurface'/>"
"      <separator/>"
"      <menuitem action='ObjectsDeleteCurrent'/>"
"    </menu>"
"    <separator/>"
"    <menu action='ComputeMenu'>"
"      <menuitem action='CompSetup'/>"
"      <separator/>"
"      <menuitem action='CompEnergy'/>"
"      <menuitem action='CompGeomOpt'/>"
"      <menuitem action='CompMolDyn'/>"
"      <menuitem action='CompRandomSearch'/>"
"      <menuitem action='CompSystematicSearch'/>"
"      <menuitem action='CompMonteCarloSearch'/>"
"      <separator/>"
"      <menuitem action='CompTorEnePlot1D'/>"
"      <menuitem action='CompTorEnePlot2D'/>"
"      <separator/>"
"      <menuitem action='CompPopAnaESP'/>"
"      <menuitem action='CompTransitionSS'/>"
"      <menuitem action='CompStationarySS'/>"
"      <separator/>"
"      <menuitem action='CompFormula'/>"
"<menuitem action='CompUC_SetFormalCharge'/>"
"<menuitem action='CompUC_CreateRS'/>"
"<menuitem action='CompUC_CycleRS'/>"
"    </menu>"
"    <menuitem action='TrajView'/>"
"    <menuitem action='SetOrbital'/>"
"    <separator/>"
"    <menu action='BuildMenu'>"
"      <menu action='BuildHydrogensMenu'>"
"        <menuitem action='BuildHydrogensAdd'/>"
"        <menuitem action='BuildHydrogensRemove'/>"
"      </menu>"
"      <separator/>"
"      <menuitem action='BuildSolvateBox'/>"
"      <menuitem action='BuildSolvateSphere'/>"
"      <separator/>"
"      <menuitem action='BuildSeqBuildAmino'/>"
"      <menuitem action='BuildSeqBuildNucleic'/>"
"      <separator/>"
"      <menuitem action='BuildCenter'/>"
"      <menuitem action='BuildClear'/>"
"    </menu>"
"    <separator/>"
"    <menuitem action='EnterCommand'/>"
"  </popup>"
"</ui>";

gtk_oglview_wnd::gtk_oglview_wnd(bool det_flag) :
	gtk_wnd(det_flag)
{
	GtkActionGroup * action_group = gtk_action_group_new("ggvActions");
	gtk_action_group_set_translation_domain(action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions(action_group, entries, G_N_ELEMENTS(entries), GTK_WIDGET(view_widget));
	
//	GtkUIManager * ui_manager = gtk_app::GetUIManager();	// does not give us independent menu widgets...
	GtkUIManager * ui_manager = gtk_ui_manager_new();
	
	gtk_ui_manager_insert_action_group(ui_manager, action_group, 0);
	
	GError * error = NULL;
	if (!gtk_ui_manager_add_ui_from_string(ui_manager, ui_description, -1, & error))
	{
		g_message(_("ERROR : Building popup-menu for gtk_oglview_wnd failed : %s"), error->message);
		g_error_free(error); exit(EXIT_FAILURE);
	}
	
	popupmenu = gtk_ui_manager_get_widget(ui_manager, "/ggvMenu");
	
	my_stereo_dialog = NULL;
	
	timer_id = 0;
}

gtk_oglview_wnd::~gtk_oglview_wnd(void)
{
	if (my_stereo_dialog != NULL)
	{
		delete my_stereo_dialog;
		my_stereo_dialog = NULL;
	}
	
	if (timer_id != 0) SetTimerOFF();
}

bool gtk_oglview_wnd::IsTimerON(void)
{
	return (timer_id != 0);
}

void gtk_oglview_wnd::SetTimerON(int msec)
{
	if (timer_id != 0) SetTimerOFF();
	
	// G_PRIORITY_HIGH_IDLE = 100
	// resizing operations = HIGH_IDLE + 10 = 110
	// redrawing operations = HIGH_IDLE + 20 = 120
	// G_PRIORITY_DEFAULT_IDLE = 200 = HIGH_IDLE + 100
	
	const gint priority = G_PRIORITY_HIGH_IDLE + 50;
	timer_id = g_timeout_add_full(priority, msec, (GSourceFunc) TimerHandler, view_widget, NULL);
}

void gtk_oglview_wnd::SetTimerOFF(void)
{
	if (timer_id != 0)
	{
		g_source_remove(timer_id);
		timer_id = 0;
	}
}

gboolean gtk_oglview_wnd::TimerHandler(GtkWidget * widget)
{
	gtk_wnd * wnd = iv_Find(widget);
	if (!wnd) return TRUE;
	
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	if (!wcl) return TRUE;
	
	GLfloat anim[3] = { wcl->animX, wcl->animY, 0.0 };
	custom_transformer_client * ctc = custom_app::GetAppC()->GetPrj();
	
	if (ctc->tc_object_ref != NULL)
	{
		ctc->tc_object_ref->OrbitObject(anim, * wcl->GetCam());
		wnd->RequestUpdate(true);
	}
	
	return TRUE;
}

// the popup-menu callbacks start here ; the popup-menu callbacks start here
// the popup-menu callbacks start here ; the popup-menu callbacks start here
// the popup-menu callbacks start here ; the popup-menu callbacks start here

void gtk_oglview_wnd::popup_ProjOrthographic(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
	wcl->cam->ortho = true;
	//wcl->cam->stereo_mode = false;
	//wcl->cam->stereo_relaxed = false;
	
	gtk_app::GetPrjX()->UpdateGraphicsViews(wcl->cam);
}

void gtk_oglview_wnd::popup_ProjPerspective(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
	wcl->cam->ortho = false;
	//wcl->cam->stereo_mode = false;
	//wcl->cam->stereo_relaxed = false;
	
	gtk_app::GetPrjX()->UpdateGraphicsViews(wcl->cam);
}

void gtk_oglview_wnd::popup_ProjSterRedBlue(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	gtk_oglview_wnd * xwnd = dynamic_cast<gtk_oglview_wnd *>(wnd);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
	// this works as a toggle item!!!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	if (wcl->cam->stereo_mode && !wcl->cam->stereo_relaxed)
	{
		wcl->cam->stereo_mode = false;		// turn OFF..
		wcl->cam->stereo_relaxed = false;
		
		if (xwnd->my_stereo_dialog)
		{
			gtk_stereo_dialog * tmpdlg = xwnd->my_stereo_dialog;
			xwnd->my_stereo_dialog = NULL;	// disconnect...
			
			tmpdlg->CloseDialog();
			delete tmpdlg;
		}
	}
	else
	{
		wcl->cam->stereo_mode = true;		// turn ON!!!
		wcl->cam->stereo_relaxed = false;
		
		if (!xwnd->my_stereo_dialog) xwnd->my_stereo_dialog = new gtk_stereo_dialog(xwnd);
		xwnd->my_stereo_dialog->SetScaling(0.25);
	}
	
	gtk_app::GetPrjX()->UpdateGraphicsViews(wcl->cam);
}

void gtk_oglview_wnd::popup_ProjSterRelaxed(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	gtk_oglview_wnd * xwnd = dynamic_cast<gtk_oglview_wnd *>(wnd);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
	// this works as a toggle item!!!
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	if (wcl->cam->stereo_mode && wcl->cam->stereo_relaxed)
	{
		wcl->cam->stereo_mode = false;		// turn OFF..
		wcl->cam->stereo_relaxed = false;
		
		if (xwnd->my_stereo_dialog)
		{
			gtk_stereo_dialog * tmpdlg = xwnd->my_stereo_dialog;
			xwnd->my_stereo_dialog = NULL;	// disconnect...
			
			tmpdlg->CloseDialog();
			delete tmpdlg;
		}
	}
	else
	{
		wcl->cam->stereo_mode = true;		// turn ON!!!
		wcl->cam->stereo_relaxed = true;
		
		if (!xwnd->my_stereo_dialog) xwnd->my_stereo_dialog = new gtk_stereo_dialog(xwnd);
		xwnd->my_stereo_dialog->SetScaling(0.50);
	}
	
	gtk_app::GetPrjX()->UpdateGraphicsViews(wcl->cam);
}

void gtk_oglview_wnd::popup_RenderQuickUpdate(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
	wcl->quick_update = !wcl->quick_update;
}

void gtk_oglview_wnd::popup_ViewsAttachDetach(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
	gtk_app::GetAppX()->AttachDetachView(wcl);
}

void gtk_oglview_wnd::popup_ViewsNewWnd(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
	bool detached = true;
	
	gtk_app::GetPrjX()->AddGraphicsClient(wcl->ccam, detached);
}

void gtk_oglview_wnd::popup_ViewsNewCam(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
	bool detached = true;
	
	gtk_app::GetPrjX()->AddGraphicsClient(NULL, detached);
}

void gtk_oglview_wnd::popup_ViewsDeleteView(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
	gtk_app::GetPrjX()->RemoveGraphicsClient(wcl, false);
}

void gtk_oglview_wnd::popup_ViewsPushCRDSet(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
gtk_project * prjX = gtk_app::GetPrjX();
	
	prjX->PushCRDSets(1);
	
	i32s tmp1 = ((i32s) prjX->GetCRDSetCount()) - 1;
	while (tmp1 > 0) { prjX->CopyCRDSet(tmp1 - 1, tmp1); tmp1 -= 1; }
	
	int last_crdset = ((int) prjX->GetCRDSetCount()) - 1;
	prjX->SetCRDSetVisible(last_crdset, true);
	prjX->UpdateAllGraphicsViews();
}

void gtk_oglview_wnd::popup_ViewsSuperimpose(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
#ifdef ENABLE_OPENBABEL
base_app::GetAppB()->ErrorMessage("\
FIXME : superimpose conflicts with openbabel?\n\
g++ (GCC) 3.3.5 (Debian 1:3.3.5-13) 20050701\n\
compiler says superimpose is undeclared???");
#else
	f64 sum = 0.0;
	for (i32s n1 = 1;n1 < (i32s) custom_app::GetPrj()->GetCRDSetCount();n1++)
	{
		superimpose si(custom_app::GetPrj(), 0, n1);
		
		for (i32s n2 = 0;n2 < 100;n2++)
		{
			si.TakeCGStep(conjugate_gradient::Newton2An);
		}
		
		f64 rms = si.GetRMS(); sum += rms;
		cout << _("sets 0 <-> ") << n1 << _(" RMS = ") << rms << endl;
		
		si.Transform();		// modify the coordinates!!!
	}
	
	f64 average = sum / ((f64) custom_app::GetPrj()->GetCRDSetCount() - 1);
	cout << _("average RMS = ") << average << endl;
#endif	// ENABLE_OPENBABEL
	
	gtk_app::GetPrjX()->UpdateAllGraphicsViews();
}

void gtk_oglview_wnd::popup_LightsNewLight(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	oglview_wcl * wcl = dynamic_cast<oglview_wcl *>(wnd->GetClient());
	
	static const char command[] = "add light";
	new gtk_command_dialog(gtk_app::GetPrjX(), wcl, command);
}

void gtk_oglview_wnd::popup_LightsSwitchLoc(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	custom_app::GetPrj()->DoSwitchLocalLights(wnd->GetClient()->GetCam(), false);
}

void gtk_oglview_wnd::popup_LightsSwitchGlob(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	custom_app::GetPrj()->DoSwitchGlobalLights(wnd->GetClient()->GetCam(), false);
}

/*################################################################################################*/

// eof

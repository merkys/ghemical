// GTK_P2DVIEW_WND.CPP

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_p2dview_wnd.h"

#include "p2dview_wcl.h"
#include "gtk_app.h"

#include "local_i18n.h"

/*################################################################################################*/

GtkActionEntry gtk_p2dview_wnd::entries[] =
{
	{ "AttachDetachPlot2D", NULL, N_("Attach/Detach This View"), NULL, N_("Select whether this view is attached or in an independent window"), (GCallback) gtk_p2dview_wnd::popup_AttachDetach },
	{ "DeleteViewPlot2D", NULL, N_("Delete View"), NULL, N_("Delete this view"), (GCallback) gtk_p2dview_wnd::popup_DeleteView }
};

const char * gtk_p2dview_wnd::ui_description =
"<ui>"
"  <popup name='gp2dvMenu'>"
"    <menuitem action='AttachDetachPlot2D'/>"
"    <separator/>"
"    <menuitem action='DeleteViewPlot2D'/>"
"  </popup>"
"</ui>";

gtk_p2dview_wnd::gtk_p2dview_wnd(bool det_flag) :
	gtk_wnd(det_flag)
{
	GtkActionGroup * action_group = gtk_action_group_new("gp2dvActions");
	gtk_action_group_set_translation_domain(action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions(action_group, entries, G_N_ELEMENTS(entries), GTK_WIDGET(view_widget));
	
	gtk_ui_manager_insert_action_group(gtk_app::GetUIManager(), action_group, 0);
	
	GError * error = NULL;
	if (!gtk_ui_manager_add_ui_from_string(gtk_app::GetUIManager(), ui_description, -1, & error))
	{
		g_message(_("ERROR : Building popup-menu for gtk_p2dview_wnd failed : %s"), error->message);
		g_error_free(error); exit(EXIT_FAILURE);
	}
	
	// set gtk_view::popupmenu so that gtk_ogl_view::ButtonHandler() will display it...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	popupmenu = gtk_ui_manager_get_widget(gtk_app::GetUIManager(), "/gp2dvMenu");
}

gtk_p2dview_wnd::~gtk_p2dview_wnd(void)
{
}

// here are the popup menu callbacks:
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

void gtk_p2dview_wnd::popup_AttachDetach(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	p2dview_wcl * wcl = dynamic_cast<p2dview_wcl *>(wnd->GetClient());
	
	gtk_app::GetAppX()->AttachDetachView(wcl);
}

void gtk_p2dview_wnd::popup_DeleteView(GtkWidget *, gpointer data)
{
	gtk_wnd * wnd = iv_Find((GtkWidget *) data);
	p2dview_wcl * wcl = dynamic_cast<p2dview_wcl *>(wnd->GetClient());
	
	gtk_app::GetPrjX()->RemovePlottingClient(wcl);
}

/*################################################################################################*/

// eof

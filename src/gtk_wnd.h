// GTK_WND.H : write a short description here...

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

//#include "ghemicalconfig2.h"

#ifndef GTK_WND_H
#define GTK_WND_H

class gtk_wnd;

#include <oglappth/base_wnd.h>

#include <gtk/gtk.h>
#include <gtk/gtkgl.h>

#include <vector>
using namespace std;

/*################################################################################################*/

// gtk_wnd class encapsulates the properties of a GTK2-window.

class gtk_wnd :
	public base_wnd
{
	private:
	
	static vector<gtk_wnd *> iv;	// instance vector
	
	protected:
	
	GtkWidget * view_widget;
	
/**	If the "##detached" pointer is different from NULL, it means that the view is
	displayed on a separate window (and not as a part of the GtkNotebook).
*/
	GtkWidget * detached;
	
	GtkWidget * label_widget;
	GtkWidget * popupmenu;
	
	friend class gtk_project;
	
	public:
	
	gtk_wnd(bool);
	~gtk_wnd(void);
	
	static gtk_wnd * iv_Find(GtkWidget *);
	
	static void RealizeHandler(GtkWidget *, gpointer);
	static gint ExposeHandler(GtkWidget *, GdkEventExpose *);
	static gint ButtonHandler(GtkWidget *, GdkEventButton *);
	static gint MotionNotifyHandler(GtkWidget *, GdkEventMotion *);
	static gint ConfigureHandler(GtkWidget *, GdkEventConfigure *);
	
	static gint DetachedDeleteHandler(GtkWidget *, GdkEvent *);
	
	GtkWidget * GetViewWidget(void);
	
	bool IsDetached(void);
	
	void RequestUpdate(bool);		// virtual
	void RequestResize(int, int);		// virtual
	
	bool SetCurrent(void);			// virtual
	
	protected:
	
	void TitleChanged(void);		// virtual
};

/*################################################################################################*/

#endif	// GTK_WND_H

// eof

// GTK_PROGRESS_DIALOG.H : write a short description here...

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#ifndef GTK_PROGRESS_DIALOG_H
#define GTK_PROGRESS_DIALOG_H

//#include "ghemicalconfig2.h"

#include "gtk_glade_dialog.h"

/*################################################################################################*/

class gtk_progress_dialog : public gtk_glade_dialog
{
	protected:
	
	GtkWidget * dialog;
	
	GtkWidget * entry_job;
	GtkWidget * drawingarea_job; int da_h; int da_w;
	GtkWidget * progressbar_job;
	
	bool cancel;
	
	int g_n; int g_sz; int g_fill;
	double * g_data;
	
	friend class gtk_project;
	
	public:
	
	gtk_progress_dialog(const char *, bool, int, int);
	~gtk_progress_dialog(void);
	
	static void handler_Destroy(GtkWidget *, gpointer);
	static void handler_ButtonCancel(GtkWidget *, gpointer);
	
	static gboolean handler_ExposeEvent(GtkWidget *, GdkEventExpose *, gpointer);
};

/*################################################################################################*/

#endif	// GTK_PROGRESS_DIALOG_H

// eof
